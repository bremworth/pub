

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 * Created by alex witting on 23/01/17.
 * alexwitting@gmail.com
 *
 * Description:

 Create an application that can read in pairs of dates in the following
 format -

 DD MM YYYY, DD MM YYYY

 Validate the input data, and compute the difference between the two dates
 in days.

 Output of the application should be of the form -

 DD MM YYYY, DD MM YYYY, difference

 Where the first date is the earliest, the second date is the latest and the difference is the number of days.

 Input can be from a file, or from standard input, as the developer chooses.

 Provide test data to exercise the application.

 Constraints:

 The application may not make use of the platform / SDK libraries for date manipulation
 (for example Date, Calendar classes).

 The application can limit calculation on an input range of dates from 1900 to 2010
 *
 */
public class DateDifference {


    public static void main(String[] args) {
        DateDifference dateDifference = new DateDifference();

        //Can receive commands either from a file from the command line or via console input
        if (args.length == 1)
            dateDifference.readFromFile(args);
        else
            dateDifference.readCommandLineInput();
    }

    /**
     * Receives commands from command line std input
     */
    private void readCommandLineInput() {
        Scanner input = new Scanner(System.in);
        String command;
        while (true) { //Program is exited by command line quit
            command = input.nextLine();
            computeDays(command);
        }
    }

    /**
     * Can read commands from a file as per the specified format
     * @param args - the command line arguments for parsing
     */
    public void readFromFile(String args[]) {
        try {
            //Get the filepath for the commands
            Path commandfile = Paths.get(args[0]);
            List<String> lines = Files.readAllLines(commandfile, Charset.defaultCharset());
            //Read all commands from the file where each command is on a line line separated by a space
            for (String line : lines) {
                computeDays(line);
            }
        } catch (Exception e) {
            //If the commands are bad we are simply ignoring them
        }
    }


    /**
     *
     * @param line - supply the two string format dates to compute the total number of days separated
     *              So date A; DD MM YYYY and B; DD MM YYYY
     */
    public void computeDays(String line) {
        String[] dates = line.split(", ");

        try {
            MyDate date1 = new MyDate(dates[0]);
            MyDate date2 = new MyDate(dates[1]);

            int days;

            if(date1.getYear() < date2.getYear()) {
                days = date1.countDifferenceInDays(date2);
                System.out.println(date1.toString() + ", " + date2.toString() + ", " + days);
            } else {
                days = date2.countDifferenceInDays(date1);
                System.out.println(date2.toString() + ", " + date1.toString() + ", " + days);
            }

        } catch (Exception e) {
            System.err.println("Date format error, please provide dates in DD MM YYYY format");
        }

    }

}
