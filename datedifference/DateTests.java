


import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


/**
 * Created by alex on 23/01/17.
 */
public class DateTests {

    public static void main(String args[]) {

        String testArgs1 = "";
        String testArgs2 = "01";
        String testArgs3 = "99 99 9999, 99 99 1111";
        String testArgs4 = "01 011 901, 91 10 2901";
        String testArgs5 = "01 01 1901 10 10 2001";
        String testArgs6 = "01 01 1901,, 10 10 2001";
        String testArgs7 = "01 01 1901,10 10 2001";
        String testArgs8 = " 01 01 1901, 10 10 2001 ";
        String testArgs9 = "01 01 2001, 10 10 1910";
        String testArgs10 = "01 01 1901, 10 10 2001";

        DateDifference date = new DateDifference();
        String error = "Date format error, please provide dates in DD MM YYYY format\n";

        System.out.println("Running Tests");
        date.readFromFile(new String[]{"date_input"});


        final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

        System.setErr(new PrintStream(errContent));


        System.out.println("Test 1");
        date.computeDays(testArgs1);
        assert errContent.toString().contains(error);

        System.out.println("Test 2");
        date.computeDays(testArgs2);
        assert errContent.toString().contains(error);

        System.out.println("Test 3");
        date.computeDays(testArgs3);
        assert errContent.toString().contains(error);

        System.out.println("Test 4");
        date.computeDays(testArgs4);
        assert errContent.toString().contains(error);

        System.out.println("Test 5");
        date.computeDays(testArgs5);
        assert errContent.toString().contains(error);

        System.out.println("Test 6");
        date.computeDays(testArgs6);
        assert errContent.toString().contains(error);

        System.out.println("Test 7");
        date.computeDays(testArgs7);
        assert errContent.toString().contains(error);

        System.out.println("Test 8");
        date.computeDays(testArgs8);
        assert errContent.toString().contains(error);

        System.out.println("Test 9 & 10");

        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        date.computeDays(testArgs9);
        assert outContent.toString().contains("32956");
        date.computeDays(testArgs10);
        assert outContent.toString().contains("36807");


        System.out.println("All Tests Completed");
    }


}
