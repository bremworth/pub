


/**
 * Created by alex on 23/01/17.
 */
public class MyDate {

    private int day;
    private int month;
    private int year;

    private static final int YEAR_START = 1900;
    private static final int YEAR_END = 2010;
    private static final int[] DAYS_IN_MONTH = {31,28,31,30,31,30,31,31,30,31,30,31};



    public MyDate(String date) throws NumberFormatException {
        int[] new_date = parseNewDate(date);

        if(validateDate(new_date)) {
            this.day = new_date[0];
            this.month = new_date[1];
            this.year = new_date[2];
        }
    }


    private int[] parseNewDate(String date) throws NumberFormatException {
        String[] date_format = date.split(" ");
        int[] new_date = new int[3];

        new_date[0] = Integer.parseInt(date_format[0]);
        new_date[1] = Integer.parseInt(date_format[1]);
        new_date[2] = Integer.parseInt(date_format[2]);

        return new_date;
    }



    public int countDifferenceInDays(MyDate date)  {

        int date_sum_1 = julianDayNumber(this);
        int date_sum_2 = julianDayNumber(date);

        return date_sum_2 - date_sum_1;

    }

    /**
     * Using a simple formula to calculate the difference in Julien Day Number format
     * http://pmyers.pcug.org.au/General/JulianDates.htm
     * There could be the option of adding on the final day (current day) but is not counted in this case.
     */
    private int julianDayNumber(MyDate date) {
        int m = date.month;
        int d = date.day;
        int y = date.year;
        if ( m > 2 ) {
            m = m - 3;
        } else {
            m = m + 9;
            y = y - 1;
        }
        int  c = y / 100;
        int ya = y - 100 * c;
        int j = (146097 * c) / 4 + (1461 * ya) / 4 + (153 * m + 2) / 5 + d + 1721119;

        return j;
    }

    private boolean validateDate(int[] date) {
        int day = date[0];
        int month = date[1];
        int year = date[2];

        //The years a restricted to between 1900 and 2010
        if(year > YEAR_END || year < YEAR_START) {
            throw new NumberFormatException("Bad Year Format: Max year accepted is 1900 and min is 2010");
        }
        if(month > 12 || month < 1) {
            throw new NumberFormatException("Bad Date Month Format");
        }
        if(day > DAYS_IN_MONTH[month] || day < 1) {
            throw new NumberFormatException("Bad Date Day Format");
        }
        return true;
    }

    public int getYear() {
        return year;
    }

    public String toString() {
        return String.format("%02d", day) + " " + String.format("%02d", month) + " " + year;
    }


}
