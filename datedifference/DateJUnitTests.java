


import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;


/**
 * Created by alex on 23/01/17.
 */
public class DateJUnitTests {

    String testArgs1 = "";
    String testArgs2 = "01";
    String testArgs3 = "99 99 9999, 99 99 1111";
    String testArgs4 = "01 011 901, 91 10 2901";
    String testArgs5 = "01 01 1901 10 10 2002";
    String testArgs6 = "01 01 1901,, 10 10 2003";
    String testArgs7 = "01 01 1901,10 10 2004";
    String testArgs8 = " 01 01 1901, 10 10 2005 ";
    String testArgs9 = "01 01 2001, 10 10 1910";
    String testArgs10 = "01 01 1901, 10 10 2001";
    String error = "Date format error, please provide dates in DD MM YYYY format\n";
    DateDifference date;
    PrintStream stdout;
    PrintStream stderr;

    @Before
    public void setup() {
        stdout = System.out;
        stderr = System.err;
        date = new DateDifference();
    }

    @After
    public void cleanUpStreams() {
        System.setOut(stdout);
        System.setErr(stderr);
    }

    @Test
    public void badInputTests() {


        final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

        System.setErr(new PrintStream(errContent));

        System.out.println("Test 1");
        date.computeDays(testArgs1);
        assertTrue(errContent.toString().contains(error));

        System.out.println("Test 2");
        date.computeDays(testArgs2);
        assertTrue(errContent.toString().contains(error));

        System.out.println("Test 3");
        date.computeDays(testArgs3);
        assertTrue(errContent.toString().contains(error));

        System.out.println("Test 4");
        date.computeDays(testArgs4);
        assertTrue(errContent.toString().contains(error));

        System.out.println("Test 5");
        date.computeDays(testArgs5);
        assertTrue(errContent.toString().contains(error));

        System.out.println("Test 6");
        date.computeDays(testArgs6);
        assertTrue(errContent.toString().contains(error));

        System.out.println("Test 7");
        date.computeDays(testArgs7);
        assertTrue(errContent.toString().contains(error));

        System.out.println("Test 8");
        date.computeDays(testArgs8);
        assertTrue(errContent.toString().contains(error));

    }

    @Test
    public void reversedInputTests() {

        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        date.computeDays(testArgs9);
        assertTrue(outContent.toString().contains("32956"));
    }

    @Test
    public void correctInputTests() {

        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        date.computeDays(testArgs10);
        assertTrue(outContent.toString().contains("36807"));


        System.out.println("All Tests Completed");
    }

    @Test
    public void readFromFileTest() {
        date.readFromFile(new String[]{"date_input"});
        //TODO
    }


}
