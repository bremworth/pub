
var answers = require('../services/score_answers');
var comments = require('../services/score_comments');
var questions = require('../services/score_questions');
var dao = require('../model/dao');
var dataSchema = require('../model/schemas/dataset');

var userids = {}; //TODO add list of gathered id's...
var answerdata = {};
var commentdata = {};
var questiondata = {};


var model = new dataSchema();

function getDataSet() {
    for(id in userids) {
           answerdata = answers.getAnswerData(id, 'nodejs');
           /*
           total_answers: 0,
            accepted_answers: 0,
            unaccepted_answers: 0,
            total_questions: 0,
            total_score: 0,
            question_score:0,
            total_implied_views: 0,
            total_activity_period: 0
            */
           commentdata = comments.getCommentsData(id);
           /*
            total_score: 0,
            post_score: 0
            */
           questiondata = questons.getQuestionData(id);
           /*
             total_questions: 0,
             answered_questions: 0,
             unanswered_questions: 0,
             total_answers: 0,
             total_score: 0,
             total_views: 0,
             total_activity_period: 0
           */
           //Processing the data for the weighted output from this point
           //processDataSet(id, answerdata, commentdata, questiondata);
           
           //Creating the raw dataset for production purposes 
           insertRawDataSet(id, answerdata, commentdata, questiondata);
    }
}


exports.insertRawDataSet = function (id, answerdata, commentdata, questiondata) {
         var model = new dataSchema({
        //var answer_model = new   answerSchema({  
                userid                  : id,
                expertise               : 'nodejs', 
                questions_count         : questiondata.total_questions,
                questions_answered      : questiondata.answered_questions,
                questions_unanswer      : questiondata.unanswer_questions,
                question_view_count     : questiondata.total_views,
                question_answer_count   : questiondata.total_answers,
                question_total_score    : questiondata.total_score,
                question_duration       : questiondata.total_activity_period,
                //});
         //var answer_model = new   answerSchema({  
                answers_total           : answerdata.total_answers,
                answers_accepted        : answerdata.accepted_answers,
                answers_unaccepted      : answerdata.unaccepted_answers,
                answers_score           : answerdata.total_score,
                answers_imp_views       : answerdata.total_implied_views,
                answers_duration        : answerdata.total_activity_period,
                //});
                comment_score           : commentdata.total_score,
                comment_post_score      : commentdata.post_score
                
            });
           var data = dao.insert(model);
}

exports.getRawDataSet = function() {
    var data = dao.find_all(dataSchema, {limit: 20});
    return data;
}

exports.getIDRawDataSet = function(id) {
    var data = dao.find_one(dataSchema, {userid: id});
    return data;
}

exports.getExpertsRawDataSet = function(experts) {
    var data = dao.find_one(dataSchema, {expertise: experts});
    return data;
}