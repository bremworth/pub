/**
 * Created by alex on 16/10/15.
 */

/**
 *
 * @param _complex_data
 * @param globaldata - custom values or loaded values
 * @param dataset
 * @param segment
 * @returns _complex_data
 */
exports.processPart = function (_complex_data, globaldata, dataset, segment) {

    var scoredata = scoringData(globaldata);

    var score = 0;

    if(_complex_data[segment] == null)
        _complex_data = {
            'question_' : [0,0,0,0,0,0],
            'answer_' : [0,0,0,0,0,0],
            'comment_' : [0,0,0,0]
        };

    _complex_data[segment][0]++;


    switch (segment) {
        case 'answer_':
            score = answer(dataset,  scoredata);
            if(score)
                _complex_data[segment][score]++;
            break;
        case 'question_':
            score =  question(dataset, scoredata);
            if(score)
                _complex_data[segment][score]++;
            break;
        case 'comment_':
            score =  comment(dataset,  scoredata);
            if(score)
                _complex_data[segment][score]++;
            break;
        default:
            break;
    }


    return _complex_data;

};

function scoringData(data) {
    var builddata = {'question_list' : {}, 'answer_list' : {}, 'comment_list' : {}};
    // - add external access
    var custom = {min_coms: 10 , high_views: 80, low_views: 20, high_ans: 80, low_ans: 20, long: 5, short: 1};

    builddata['question_list'].score = (data['question_list'].score[1] /
        data['question_list'].score[2]);
    builddata['question_list'].view_count = {};
    builddata['question_list'].view_count.avg = (data['question_list'].view_count[1] /
        data['question_list'].view_count[2])
    builddata['question_list'].view_count.is_high = (data['question_list'].view_count[1] /
        data['question_list'].view_count[2]) / 100 * custom.high_views;
    builddata['question_list'].view_count.is_low = (data['question_list'].view_count[1] /
        data['question_list'].view_count[2]) / 100 * custom.low_views;
    // **Answer counts are separated for now - need more info/research on the value
    builddata['question_list'].answer_count = {};
    builddata['question_list'].answer_count.is_high = (data['question_list'].answer_count[1] /
        data['question_list'].answer_count[2]) / 100 * custom.high_ans;
    builddata['question_list'].answer_count.is_low = (data['question_list'].answer_count[1] /
        data['question_list'].answer_count[2])  / 100 * custom.low_ans;
    builddata['question_list'].active_duration = {};
    builddata['question_list'].active_duration.is_short = (((data['question_list'].active_duration[1] /
        data['question_list'].active_duration[2]) / 1000 / 3600 / 24) < custom.short);
    builddata['question_list'].active_duration.is_long  = (((data['question_list'].active_duration[1] /
        data['question_list'].active_duration[2]) / 1000 / 3600 / 24) > custom.long);

    //answers
    builddata['answer_list'].score = (data['answer_list'].score[1] /
        data['answer_list'].score[2]);
    builddata['answer_list'].parent_score = (data['answer_list'].parent_score[1] /
        data['answer_list'].parent_score[2]);
    builddata['answer_list'].parent_view_count = (data['answer_list'].parent_view_count[1] /
        data['answer_list'].parent_view_count[2]);
    builddata['answer_list'].parent_active_duration = {};
    builddata['answer_list'].parent_active_duration.is_short = (((data['answer_list'].parent_active_duration[1] /
        data['answer_list'].parent_active_duration[2]) / 1000 / 3600 / 24) < custom.short);
    builddata['answer_list'].parent_active_duration.is_long = (((data['answer_list'].parent_active_duration[1] /
        data['answer_list'].parent_active_duration[2]) / 1000 / 3600 / 24) > custom.long);

    //comments
    builddata['comment_list']['min_score'] = custom.min_coms;
    builddata['comment_list'].score =  (data['comment_list'].score[1] /
            data['comment_list'].score[2]);

   // console.log(builddata);

    return builddata;
}

function question(question,  scoredata) {
    /**
     questionSchema
     id: Number,
     score: Number,
     view_count: Number,
     is_answered: Boolean,
     answer_count: Number,
     active_duration: Number
     */
    var sd = scoredata['question_list'];

    /*
     * While it is hard to measure the expert level of a user based off a question as people who are experts
     * may be involved in a lot of questions that gather little traffic. The idea is to go for consistency.

    if the question is answered - worthy question
        If the question has a high score -- good question
            if the question has high answers count - tricky question
                if the question has high view count -- famous question - or v.high
                else has low view count (with high answers + high score) -- technically specific
                    If the duration was quick - relevant/on topic question
                    else the duration was long - hard question
                        else the question is not answered - technically hard

     */

    var highvotes = question.score > sd.score;
    var votesViews = ((question.view_count  > sd.score) && ((question.view_count / 100 * 70) < sd.score)); //70% every view voted
    var viewshigh = question.view_count > sd.view_count.is_high;
    var viewslow = question.view_count < sd.view_count.is_low;

    // **Answer counts are separated for now - need more info/research on the value
    var anshigh = question.answer_count > sd.answer_count.is_high;
    var anslow = question.answer_count < sd.answer_count.is_low;

    var long = question.active_duration < sd.active_duration.is_short; //very quick
    var short = question.active_duration > sd.active_duration.is_long; //very long

    if(question.is_answered) {
        //The first to questions give the best result. They are designed to be specific technical questions
        //That give discussion or a popular and on topic
        if(highvotes) {
            //high views, a high answer count and long time to answer - expertise question
            if((viewshigh && anshigh && long) || votesViews)
                return 1; //expert discussion

            if(viewslow && long)
                return 2; //Expertise discussion

            //high views, a high\low answer count and quickly answered - famous question
            if(viewshigh && short && anshigh)
                return 3; //Popular discussion

            //low views, a low answer count and quickly answered
            if(viewslow && short && anslow)
                return 4; //direct discussion - on topic - possibly a specific field - professionals

            return 5; //good discussion
        }
        if(!highvotes) {
            if(votesViews)
                return 2;
            //low votes, high views, a high/low answer count and long time to answer
            if((viewshigh && anshigh && long) || (viewshigh && short))
                return 3; //famous discussion
        }
    }

    if(!question.is_answered) {
        if(highvotes) {
            if ((viewslow && short) || viewshigh)
                return 2; //Bounties? - expertise discussion
        }
    }

    return 0;
}

function answer(answer, scoredata) {

    /**
     answerSchema
     id: Number,
     score: Number,
     is_accepted: Boolean,
     parent_id: Number,
     parent_score: Number,
     parent_view_count: Number,
     parent_active_duration: Number
     */
    var sd = scoredata['answer_list'];

    /*

     If the answer is accepted - solid
        if a high score - good answer
            if a short duration - quick thinker answer
                if a high parent score - even better answer
                    if a high parent view count - famous answer
                    *note - cannot tell how many answers there were for the question
     */

    var a = answer.score > sd.score;
    var b = answer.parent_score > sd.parent_score;
    var c = answer.parent_view_count > sd.parent_view_count;
    var d = answer.parent_active_duration < sd.parent_active_duration.is_short; //very quick
    var e = answer.parent_active_duration > sd.parent_active_duration.is_long; //very long


    if(answer.is_accepted) {
        // high scoring question and answer and was either quick to answer or gave the correct answer late
        if((a && b && c && d) || (a && b && c && e))
            return 1; //expert_answerer
        //high score that was quick to answer or remained unanswered for a long time
        if((a && !b && !c && e) || (a && !b && !c && d))
            return 2; //expertise_answer
        //High traffic answer that scored well
        if((a && b && c) || (a && b && d) || (a && c && d) ||  (a && b && e) || (a && c && e) )
            return 3; //famous/popular_answer
        //Scored well
        if ((a && b) || (a && c) || (c && b) || a)
            return 4; //great_answerer
        //Points
        if(b || c)
            return 5; //contributor
    }
    /*
     if the answer is not accepted
        has a very high score - possible better answer (cannot confirm)
            if the parent duration was very short - again an indicator of possible better answer (asker quickly accepted first answer)
                if the parent has a high view count - better answer
                    if the parent has a high view count
     */
    //If the answer is not accepted the requirements for scoring are a little higher.
    if(!answer.is_accepted) {
        //The answer took a while to be answer and users answer is regarded as potentially better or important
        if(a && b && c && e)
            return 1;
        //The answer took a while to be answer and users answer is regarded as potentially better or important
        if(a && e)
            return 2;
        //Knows the topic well enough to provide an answer when others exist
        if(a && b && d)
            return 4;
    }

    return 0;
}


function comment(comment, scoredata) {
    /**
     * commentSchema
     *   id: Number,
     *   score: Number,
     *   parent_id: Number,
     *   parent_score: Number
     */
    var sd = scoredata['answer_list'];
    if(comment.score > sd.min_score) {
        if(comment.score > sd.score) {
            if(comment.score > comment.parent_score) {
                return 1;
            }
            return 2;
        }
        return 3;
    }

    return 0;
}

/**
 * Once the individual sub segments have been processed the data is returned and evaluated
 * on a per segment level for all sub - parts. Once these segments are examined the overall
 * data is evaluated in relation to all parts and scores on a higher abstraction
 * @param _complex_data - the scored data
 * @param callback
 * @param parameters - the badge configuration -
 */
exports.evaluation = function(_complex_data, parameters, callback) {

    var question = _complex_data['question_'];
    var answer = _complex_data['answer_'];
    var comment = _complex_data['comment_'];

    console.log(question);
    console.log(answer);
    console.log(comment);

    question.shift();
    answer.shift();
    comment.shift();
    //q1,q2,q3,q4,q5,a1,a2,a3,a4,a5,c1, c2, c3
    // 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
    var r1 = question.concat(answer);
    var rank = r1.concat(comment);

    //We generally want the answers to be first -> questions -> comments
    //we rebuild to maintain indices
    for(var v = 0; v < rank.length; ++v) {
        rank[v] = [rank[v],v]; //[score,old_index]
    }

    //custom sorting
    for(var i = 0; i < rank.length; ++i) {
        for(var j = i+1; j < rank.length; ++j) {
            if(rank[i][0] < rank[j][0]) { //we examine value pairs
                var temp = rank[i];
                rank[i] = rank[j];
                rank[j] = temp;
            }
        }
    }

    console.log(rank);


    var badges = {      //q1,q2,q3,q4,q5,a1,a2,a3,a4,a5,c1, c2, c3
                        // 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
        //The order in which we want the ranked order to be, they can be set to required or soft requirement
        //so they can be out of place. Generally the idea would be to have the first 4 or 5 as hard requirements
        //and then the rest of the order can be a soft requirement or out of place or even not needed.

        'Professor'  : [[5,1], [0,1], [7,1],[12,1],[6,1],[3,1],[4,1],[8,1],[2,1],[9,1],[10,1],[11,1],[1,1]],
        'Researcher' : [[7,1], [5,1], [2,1], [6,1], [9,1], [4,1], [3,1], [1,1], [8,1], [0,1], [10,1], [11,1], [12,1]], //[[5,1], [6,1], [9,1], [3,1], [4,1], [0,1], [1,1], [7,1], [8,1], [2,1], [10,1], [11,1], [12,1]],
        'Knowledge'  : [[2,1], [1,1], [0,0], [6,1], [7,1], [5,0], [3,0], [4,0]],
        'Leadership' : [[1,1], [6,1], [5,1], [0,1], [3,1], [7,1]],
        'Analytical' : [[2,1], [1,1], [4,1], [10,1], [11,1], [12,0], [5,0], [7,0], [8,0], [9,0], [10,0], [11,0], [12,0]]
    };

    if(!parameters)
        parameters = badges;

    //some parameters need to be fix in their position and order
    //if there are 0,1,2,3 these may need to be in place and only in place
    //while others may not need to be in place
    //so if (hard) requirement and z != rank.length
    var awarded = [];
    for(var key in parameters) {
        var r = 0;
        var z = 0;
        while (r < rank.length) {
            if (rank[r][1] == parameters[key][z][0]) {
                z++;
            }
            r++;
            if ((r != z) && parameters[key][z][1]) { // soft requirements
                break;
            }
        }
        if(z == parameters[key].length) {
            awarded.push({name: key, complexdata: parameters[key]});
        }
    }
    console.log(awarded);
    return callback(awarded);

};

module.exports.descriptions = {
    'Professor'  : 'Someone who professes.',
    'Knowledge'  : 'Someone who has given a volume of work and has been recognised demonstrating their knowledge.',
    'Researcher' : 'Looking for answers and giving solutions. Asking for insights that are often rewarded with lengthy discussion',
    'Leadership' : 'Someone who delivers topics of importance that few can answer and helps solve problems of long debate',
    'Analytical' : 'Awarded for those who contribute after the fact, willing to give analysis and further information.'
};


/*
function test() {
    var ps = {'professor': [[5,1],[0,1],[7,1],[12,1],[6,1],[3,1],[4,1],[8,1],[2,1],[9,1],[10,1],[11,1],[1,1]]};

    var complextest = {
        question_list: [12,5,1,2,2,2],
        answer_list: [17,7,3,4,2,1],
        comment_list: [6,1,1,4]
    };

    this.evaluation(complextest, ps, function(result) {
        console.log('Awarded');
        console.log(result);
    });

}
*/
//test();







