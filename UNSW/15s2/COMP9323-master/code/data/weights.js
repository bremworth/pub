/**
 * Created by alex on 15/10/15.
 */

var WeightTableSchema = require('../model/schemas/weighttable');
var dao = require('../model/dao');

/**
 * Create a new weight table from the given parameters
 * If parameters are not given they default to the standard (hardcoded) parameters
 * @param body
 * @returns {WeightsTable|exports|module.exports}
 */
exports.buildTable = function(body) {
    return new WeightTableSchema({
        configuration: body.configuration,
        active: false,
        question_list : {
            score           : body.question_list.score || 5,
            view_count      : body.question_list.view_count || 5,
            is_answered     : body.question_list.is_answered || 5,
            answer_count    : body.question_list.answer_count || 5,
            active_duration : body.question_list.active_duration || 5
        },
        answer_list :{
            score                    : body.answer_list.score || 5,
            parent_score             : body.answer_list.parent_score || 5,
            parent_view_count        : body.answer_list.parent_view_count || 5,
            parent_active_duration   : body.answer_list.parent_active_duration || 5
        },
        comment_list : {
            score          : body.comment_list.score || 5,
            parent_score   : body.comment_list.parent_score || 5
        },
        question_score           : body.question_score || 15,
        answer_score             : body.answer_score || 15,
        comment_score            : body.comment_score || 15
    })
};

/**
 * Get the active table or a specified weight table or return a new table
 * @param callback - returns the table
 */
exports.getWeightTables = function(table, callback) {
    //The default - this change be switched to a named config
    var options = {active: true};
    //Or a named configuration
    if(table != '')
         options = {configuration: table};

    //Get the weight tables
    dao.find_one(WeightTableSchema, options, function(result) {
        //If no weight table exists create the default
        if(result == null) {
            result = createTable();
            dao.insert(result, function() {
                console.log('Initialised default weights')
                return callback(result);
            })
        } else {
            return callback(result);
        }
    })
};

/**
 * Updates the table configuration with the given parameters
 * If no new parameters are given the remain unchanged
 * @param body
 * @param tableData
 * @param callback
 */
exports.updateTable = function(body, tableData, callback) {
    dao.find_one_update(WeightTableSchema, {configuration: tableData.configuration},
        {
            question_list : {
                score           : body.question_list.score || tableData.question_list.score,
                view_count      : body.question_list.view_count || tableData.question_list.view_count,
                is_answered     : body.question_list.is_answered || tableData.question_list.is_answered,
                answer_count    : body.question_list.answer_count || tableData.question_list.answer_count,
                active_duration : body.question_list.active_duration || tableData.question_list.active_duration
            },
            answer_list :{
                score                    : body.answer_list.score ||  tableData.answer_list.score,
                parent_score             : body.answer_list.parent_score || tableData.answer_list.parent_score,
                parent_view_count        : body.answer_list.parent_view_count || tableData.answer_list.parent_view_count,
                parent_active_duration   : body.answer_list.parent_active_duration || tableData.answer_list.parent_active_duration
            },
            comment_list : {
                score          : body.comment_list.score || tableData.comment_list.score,
                parent_score   : body.comment_list.parent_score || tableData.comment_list.parent_score
            },
            question_score           : body.question_score || tableData.question_score,
            answer_score             : body.answer_score || tableData.answer_score,
            comment_score            : body.comment_score || tableData.comment_score
        }, function (result) {
            return callback(result);
    });
}

function createTable() {
    /*
     The hardcoded weightings in the default table. These can be update through the api
     or new tables can be created
     */
    return new WeightTableSchema({
        configuration: 'default',
        active: true,
        question_list : {
            score           : 5,
            view_count      : 5,
            is_answered     : 5,
            answer_count    : 5,
            active_duration : 5
        },
        answer_list :{
            score                    : 5,
            parent_score             : 5,
            parent_view_count        : 5,
            parent_active_duration   : 5
        },
        comment_list : {
            score          : 5,
            parent_score   : 5
        },
        question_score           : 15,
        answer_score             : 15,
        comment_score            : 15
    })
}
