var SO_auth = require('../SO_auth/SO_auth.js');

/** STEP 1: call this only to get url **/
SO_auth.getAuthURL(function(err, url){
    console.log(url);
});


/** testing for SO access token itself (included in the linkProfile function) **/
//SO_auth.getSOAccessToken('efEwVeRb4fisXqETrjOLJQ))', function(access_token){
//    console.log(access_token);
//});


/** STEP 2: call this with the code **/
//SO_auth.linkProfile('123','VHw*7c7iKOyQQzaXIy0Xpg))');


