/**
 * Created by Michael on 9/28/2015.
 * Install: npm install stormpath
 * It is required to create account in stormpath and then store keyxxx.properties
 * For more information go to 
 * www.stormpath.com
 * http://docs.stormpath.com/nodejs/quickstart/
 */
var stormpath = require('stormpath');
var keyfile = 'C:/Stormpath/' + 'apiKey-39TH48LGIETF5TMZLT5CVAJH0.properties';
client = null;
var accounts = {
    username: 'tk455',
    password: 'Changeme1'
}

stormpath.loadApiKey(keyfile, function apiKeyFileLoaded(err, apiKey){
    if (err) throw err;
    client = new stormpath.Client({apiKey: apiKey});
    client.getApplications({name:'My Application'}, function(err, applications){
        if (err) throw err;
        app = applications.items[0];

        // Authenticate Account by username / password.
        app.authenticateAccount(accounts, function (err, result) {
            if (err) throw err;
            console.log('Successfully authenticated account using username!');
        });

        // Authenticate Account by email / password.
        app.authenticateAccount({
            username: 'stormtrooper@stormpath.com',
            password: 'Changeme1',
        }, function (err, result) {
            if (err) throw err;
            console.log('Successfully authenticated account using email!');
        });
    });
});


