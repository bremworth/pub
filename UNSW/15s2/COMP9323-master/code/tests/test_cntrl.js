/**
 * Created by alex on 13/10/15.
 */

var assert = require('assert');
var ctrl = require('../controller/controller');
var restify = require('restify');
var server = require('../controller/server');
var dao = require('../model/dao');
var userSchema = require('../model/schemas/user');
var Weights = require('../model/schemas/weights');
var userScores = require('../model/schemas/userscores');
var Experts = require('../model/schemas/badge');
var dataSchema = require('../services/dataset/datasetUserSchema');


var user = 'dataset_user_1'
var soid = '617839';
var so = 617839;

function buildData() {


    var new_user = new userSchema({
        user_id: user,
        tempLoginToken : null,
        expiry: Date.now(),
        SO_id:  soid,
        SO_access_token: null,
        type: 'stackoverflow',
        badge_ids: []
    });

    dao.insert(new_user, function() {ctrl.updateUser(user)});
    //ctrl.updateUser(user)
}


function runTest() {

    dao.find_one(userSchema, {user_id: user}, function(result) {test('user', result)});
    dao.find_one(Experts, {expertise: 'nodejs'}, function(result) {test('experts',result)});
    dao.find_one(userScores, {userid: user}, function(result) {test('scores',result)});
    dao.find_one(Weights, {name: 'Global_weights'}, function(result) {test('global weights',result)});
    dao.find_one(Weights, {name: 'nodejs'}, function(result) {test('nodejs',result)});
    //dao.find_one(dataSchema, {user_id: so}, function(result) {test('dataset',result)});
}

function test(msg, result) {
    console.log(msg);
    console.log(result);
    assert.notEqual(result, null, 'Failed');
};

/*
 To test locally run the buildData() first to create a dummy set of data
 then run the test to make sure everything is in place.
 */

//buildData();
//runTest();
