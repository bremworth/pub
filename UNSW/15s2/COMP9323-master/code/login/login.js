/*
 * login.js
 *  
 * Created by Michael on 10/6/2015.
 * 
 * Changes:
 *  Alvin Hielman - 11 Oct 2015 19:30 AEST
 *      + Implemented error first callback.
 *      + Code refactor for consistency.
 * 
 */

var randtoken = require('rand-token');
var stormpath = require('stormpath');
var User = require('../model/schemas/user');
var dao = require('../model/dao');

var keyID = "39TH48LGIETF5TMZLT5CVAJH0";
var keySecret = "MGSErcafra617ogiNdGoNEaBLElH+kUu0sNyF8eZKyM";

/*
 * This is account modules
 */
module.exports = {
    
    /*
     * This function is to create account on Stormpath.
     * Parameter givenName - given name (not required)
     * Parameter surname - surname (not required)
     * Parameter username - username (required)
     * Parameter email - email (required)
     * Parameter password - password (required)
     *                      1. Password must be 8 and 100 characters in length
     *                      2. It contains at least one lower case alphabetic character
     *                      3. It contains at least one upper case alphabetic character
     *                      4. It contains at least one number
     * Parameter displayName - display name (not required)
     *
     * Return - error
     *        - user ID - successfully creating an account.
     */
    createAccount: function(givenName, surname, username, email, password, displayName, callback) {
        
        // Create API key
        var apiKey = new stormpath.ApiKey(keyID, keySecret);
        var client = new stormpath.Client({apiKey: apiKey});
        client.getApplications(function(err, apps) {
            if(err) return callback(err);

            var userid = randtoken.generate(16);
            var account = {
                givenName: givenName,
                surname: surname,
                username: username,
                email: email,
                password: password,
                customData: {
                    displayname: displayName,
                    userid: userid
                },
            };

            var app = apps.items[0];
            // Create a Stormpath Account.
            app.createAccount(account, function (err, account) {
                if(err) return callback(err);

                // Link new user for REEPer database through user ID
                var newUser = new User({
                    user_id : userid,
                    tempLoginToken : null,
                    expiry: null,
                    type: "SO"
                });
                
                // Insert the user into REEPer database
                dao.insert(newUser, function() {
                    console.log('New user created: ' + name);
                });

                return callback(null, true, userid);
            });
        });
    },

    /*
     * This function is to login by using username and password. After successful login,
     * a user will receive REEPer token that last for 5 minutes.
     * Parameter username - username (required)
     * Parameter password - password (required)
     *
     * Return - error
     *        - REEPer token (Successful login)
     */
    login: function(username, password, callback) {
        var durationToken = 10;
        
        // Create API key
        var apiKey = new stormpath.ApiKey(keyID, keySecret);
        var client = new stormpath.Client({apiKey: apiKey});
        client.getApplications({name:'My Application'}, function(err, applications) {
            if(err) return callback(err);
            var app = applications.items[0];
            
            var account = {
                username: username,
                password: password
            };
            
            // Authenticate an account by username
            app.authenticateAccount(account, function (err, result) {
                if(err) return callback(err);
                
                var systemToken = randtoken.generate(16); // Prepare the token
                // Get user's account to place the token there
                app.getAccounts({username: account.username}, function(err, accounts) {
                    if (err) throw err;
                    accounts.each(function (acc, index) {
                        client.getAccount(acc.customData.href, function(err,acc){
                            if (err) throw err;
                            var userid = acc.userid;
                            
                            // Setup duration of token
                            var expiredDate = new Date();
                            expiredDate.setMinutes(expiredDate.getMinutes() + durationToken);
                            
                            // Place all data into user database in REEPer system
                            dao.update(User,{user_id: userid},{tempLoginToken: systemToken, expiry: expiredDate}, function(docs) {
                                if(docs) {
                                    return callback(null, systemToken);
                                }
                                return callback('Update data into database failed', false);
                            });
                        });
                    });
                });
            });
        });
    },
    
    // TODO: need to clean user badges database
    /*
     *  This function is to remove user account from stormpath and user database in 
     *  REEPer.
     *
     *  Parameter username - username
     *  Parameter password - password
     *
     *  Return - error
     *         - null (The account and the database have been erased)
     */
    remove: function(username, password, callback) {
        
        // Create API key
        var apiKey = new stormpath.ApiKey(keyID, keySecret);
        var client = new stormpath.Client({apiKey: apiKey});
        client.getApplications({name:'My Application'}, function(err, applications) {
            if (err) throw err;
            var app = applications.items[0];

            var usrInfo = {
                username: username,
                password: password
            }
            // Authenticate first
            app.authenticateAccount(usrInfo, function (err, result) {
                if (err) throw err;
                app.getAccounts({username: usrInfo.username}, function(err, accounts) {
                    if (err) throw err;
                    accounts.each(function (account, index) {
                        client.getAccount(account.customData.href, function(err, acc) {
                            if (err) throw err;
                            var userid = acc.userid;
                            
                            // Remove user database on REEPer
                            dao.find_one_remove(User, {user_id: userid}, function(docs) {
                                if (docs) {
                                    
                                    // TODO: Remove user's badges from database on REEPer
                                    
                                    // Delete stormpath account
                                    account.delete(function(err, result) {
                                        if (err) throw err;
                                        return(null);
                                    })
                                }
                                return callback('Removing user database failed', false);
                            });
                        });
                    });
                });
            });
        });
    }
}
