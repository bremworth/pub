// To run the code in the terminal: node client.js



var restify = require('restify');

var service = restify.createJsonClient({
    url: 'https://api.stackexchange.com/2.2',
    version: '*'
});

// service to get all questions for user with id 548225
service.get('/users/548225/questions?site=stackoverflow', function(err, req, res, obj) {
    
    for(var itemKey in obj.items){
        //prints the tag array to console
        console.log(obj.items[itemKey].tags);

        for(var tagKey in obj.items[itemKey].tags){
            //prints individual tag in an array to console
            console.log(obj.items[itemKey].tags[tagKey]);
        }
        
        console.log()
    }

});
