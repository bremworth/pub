/**
 * Created by Udeeksh on 28/10/2015.
 */
//var platform = require('../../model/schemas/platform');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var dbRemoteServerIP = "52.10.183.118:27017";
mongoose.connect(dbRemoteServerIP);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

var platformSchema = new Schema({
    name        : String,
    expertiseArea   : Array,
    expertiseBadges : Array
});


//Compile model
var Platform = mongoose.model('Platform', platformSchema);

function updatePlatform(){
    console.log("Saving to platform.");
    var platformData = new Object();
    platformData['name'] = "StackOverflow";
    platformData['expertiseArea'] = ['python','java','node.js','c++','javascript'];
    platformData['expertiseBadges'] = ['proficient','skilled','talented','expert'];
    //console.log("The object: ",platformData);
    var platformModel = new Platform(platformData);

    //console.log("The model: ",platformModel);
    platformModel.save(function (err, result){
        if (err){
            console.log("Couldn't save the data",err);
            return;
        }
        console.log("Platform updated successfully.",result);
    });
}

updatePlatform();
