/*
 * Created by Michael on 10/2/2015.
 * 1. npm install request
 * 2. Experiment on http://stackoverflow.com/questions/32674280
 * 3. Idea:
 *    Answer Id is provided
 *    Query answer{id} from SO and get Question Id
 *    Fetch html content for question
 *    Check if content of the body contains: div class=\"bounty-award-container. True if it contains, else return.
 *    Split the body content by string: itemscope itemtype=\"http://schema.org/Answer
 *    if the content has string: div class=\"bounty-award-container and string a href=\"/users/" + user_id + "/" + display_name
 *    Then get the bounty
 *
 *    Remember: display name can contain ' ', you need to replace all occurences with '-'
 */

var restify = require('restify');
var service = restify.createJsonClient({
    url: 'https://api.stackexchange.com/2.2',
    version: '*'
});

var request = require('request');
var HashMap = require('hashmap');
var mapBounty = new HashMap();
var mapFavorite = new HashMap();

module.exports = {
    getBounty: function(answerID) { //32751150
        var questionQueryString = "/answers/"+answerID+"?order=desc&sort=activity&site=stackoverflow";
        service.get(questionQueryString, function (err, req, res, obj) {
            var user_id = obj.items[0].owner.user_id;

            // Change all occurence ' ' to '-'
            var re = new RegExp(' ', 'g');
            var display_name = obj.items[0].owner.display_name.toLowerCase().replace(re, '-');

            request("http://stackoverflow.com/questions/"+obj.items[0].question_id, function (err, response, body) {
                if(err) throw err;
                // if no bounty, skip it
                if (body.indexOf("div class=\"bounty-award-container\"") == -1){return;}

                var arr_str = body.split("itemscope itemtype=\"http://schema.org/Answer\"");
                for (var i = 1; i < arr_str.length; i++) {
                    if (arr_str[i].indexOf("div class=\"bounty-award-container\"") > -1 &&
                        arr_str[i].indexOf("a href=\"/users/" + user_id + "/" + display_name) > -1){
                        var bounty_sentence = arr_str[i].split("<div class=\"bounty-award-container\"");
                        bounty_sentence = bounty_sentence[1].split("</span");
                        var usr_bounty = bounty_sentence[0].substr(bounty_sentence[0].lastIndexOf("+") + 1);
                        //console.log(usr_bounty);
                        mapBounty.set(answerID, usr_bounty);
                       return map;
                    }
                }
            });
        });
    },

    getFavorite: function(questionID) { // 7563693
        request("http://stackoverflow.com/questions/"+questionID, function (err, response, body) {
            if (err) throw err;

            var fav = body.match("<div class=\"favoritecount\"><b>[0-9]+</b></div>");
            if (fav.length > 0) {
                var usr_favorite = fav[0].replace("<div class=\"favoritecount\"><b>", "").replace("</b></div>", "");
                mapFavorite.set(questionID, usr_favorite);
                console.log(usr_favorite);
                return mapFavorite;
            }
        });
    }

}
