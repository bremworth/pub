/**
 * Created by alex on 15/10/15.
 */

/*** Database WMD ***/

var mongo = require('mongodb');

var client = mongo.MongoClient;

var url = 'mongodb://52.10.183.118:27017/test';


//Do not include users or usermodels
var collect = ['userscores','badges'];

var current = 'system.indexes';
var database = null;
function deleteCollections() {
    client.connect(url, function(err, db) {
        console.log('Connected');
        database = db;
        next(0);

    });
};

function next(i) {
    if(i < collect.length) {
        var table = collect[i];
        database.collection(table).drop(function(err, response) {
            console.log('Dropped '+collect[i]+' '+response);
            next(i+1);
        });
    } else {
        database.close();
    }
}

deleteCollections();
