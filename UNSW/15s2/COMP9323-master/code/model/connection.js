/*
 * connection.js
 * Written by Alex Witting for COMP9323 Group 1 
 * 17-9-2015
 */

var mongoose = require('mongoose');

var remote = '52.10.183.118:27017';
var uri = remote || 'mongodb://localhost/';

exports.createDBconnection = function() {
   
   mongoose.connect(uri);
    
    mongoose.connection.on('connected', function () {  
         console.log('Mongoose default connection open to ' + uri);
    }); 

    mongoose.connection.on('error',function (err) {  
         console.log('Mongoose default connection error: ' + err);
    }); 

    mongoose.connection.on('disconnected', function () {  
         console.log('Mongoose default connection disconnected'); 
    });

    process.on('SIGINT', function() {  
          mongoose.connection.close(function () { 
            console.log('Mongoose default connection disconnected through app termination'); 
             process.exit(0); 
         }); 
    }); 
    
};

exports.closeDBconnection = function() {
    mongoose.connection.close();
    console.log('Connection closed');
};
