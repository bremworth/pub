
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/*
 * Stored data for learning
 * - as users information is collected min/max/average levels are modified
 */ 
var scoreDataSchema = new Schema({
        expertise: String,
        question_list : {
                score           : Array, //[Max, Total, Count]
                view_count      : Array,
                is_answered     : Array, //[Count_1, Count_0]
                answer_count    : Array,
                active_duration : Array,
        },
        answer_list :{
                score                    : Array, //[Max, Total, Count]
                is_accepted              : Array, //[Count_1, Count_0]
                parent_score             : Array,
                parent_view_count        : Array,
                parent_active_duration   : Array,
                },
        comment_list : {
                score          : Array,
                parent_score   : Array
        }
});


//Compile the models
var scoreData = mongoose.model('ScoreData', scoreDataSchema);
module.exports = scoreData;

