/*
 * userscore.js
 * Written by Alex Witting for COMP9323 Group 1 
 * 17-9-2015
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ScoreSchema = new Schema({

        userid: String,
        expertise: String,
        scoredata: {
             question_score: Array,  //[Total, Count,  Weight]
             answer_score: Array,
             comment_score: Array,
            
             complex_feature: [{
                    name: String,
                    complexdata: Array
               }]
        }
});


//Compile the model
var UserScore = mongoose.model('UserScore', ScoreSchema);

module.exports = UserScore;


    
