/**
 * Created by alex on 12/10/15.
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var platformSchema = new Schema({
    name        : String,
    expertiseArea   : Array,
    expertiseBadges : Array
});


//Compile model
var Platform = mongoose.model('Platform', platformSchema);

module.exports = Platform;

