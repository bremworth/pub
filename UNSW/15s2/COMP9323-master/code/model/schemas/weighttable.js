/**
 * Created by alex on 15/10/15.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var weightTable = new Schema({
    configuration: String,
    active: Boolean,
    question_list : {
        score           : Number,
        view_count      : Number,
        is_answered     : Number,
        answer_count    : Number,
        active_duration : Number,
    },
    answer_list :{
        score             : Number,
        parent_score      : Number,
        view_count        : Number,
        active_duration   : Number
    },
    comment_list : {
        score          : Number,
        parent_score   : Number
    },
    question_score           : Number,
    answer_score             : Number,
    comment_score            : Number
});


//Compile the models
var WeightsTable = mongoose.model('WeightsTable', weightTable);
module.exports = WeightsTable;

