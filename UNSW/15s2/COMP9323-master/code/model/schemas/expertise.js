/*
 * expertise.js
 * Written by Alex Witting for COMP9323 Group 1 
 * 17-9-2015
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var expertiseSchema = new Schema({
    expertid        : Number,
    name            : String,
    totalscore      : Number,
    answerscore     : Number,
    commentscore    : Number,
    questionscore   : Number
});


//Compile model
var Expertise = mongoose.model('Expertise', expertiseSchema);

module.exports = Expertise;




  

    
