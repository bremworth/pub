/*
 * badge.js
 * Written by Alex Witting for COMP9323 Group 1 
 * 17-9-2015
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var badgeSchema = new Schema({
    userids     : Array,   //award to.. for when searching
    name        : String,
    expertise   : String,  //expertise area or custom name
    description : String,  //Text description
    min_score   : Number,
    type        : String, //custom, default
    feature     : String, //awarded by a specific feature
    multi       : Array  //for multiple requirements
});


//Compile model
var Badge = mongoose.model('Badge', badgeSchema);

module.exports = Badge;




  

    
