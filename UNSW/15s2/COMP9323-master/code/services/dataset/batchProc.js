/**
 * Created by Udeeksh on 24/10/2015.
 */
/* Module to populate the dataset for multiple users. 
* The input can be configured in batchInput.js and is required by this module
*
 */

var mongoose = require('mongoose');
var userData = require('./getUserData.js');
var datasetUser = require('./datasetUserSchema.js');
var inputBatchHandle = require('./batchInput.js');

var userSavedCtr = 0;

var dbRemoteServerIP = "52.10.183.118:27017";
//mongoose.connect('mongodb://localhost/test');
mongoose.connect(dbRemoteServerIP);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

/*
* Function to get the data and save single user
* Data fecth for each User requires (1+2+4) = 7 API calls to SO 
*/
function daoFetchAndSaveUser(userId, expertiseArea){
    userData.getData(userId, expertiseArea, function (model) {
        console.log("Feature extraction completed successfully.");
        //checkDataReceived(model); //Uncomment this function to log the detailed object

        var datasetObject = new datasetUser.userModel(model);
        datasetObject.save(function (err, result){
            if (err){
                console.log("Couldn't save userId:",userId);
                return;
            }
            console.log("Data for userId:",userId,"Saved successfully.");
            userSavedCtr++;
            console.log("Total Users saved: ",userSavedCtr);

        });
    });
}

/*
* Controller Function to initiate and process the batch input
* Injects a forceable delay of 15 seconds between API calls for each user.
* 
*/
var flatBatchInput = inputBatchHandle.flatInput();
var ctr = 0;
var waitPeriod = 15000;
function fetchAndSaveBatchInput(){
    var timestamp = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
    var tuple = flatBatchInput[ctr];
    console.log(timestamp, ": Initiating StackOverflow Feature Extraction \nUser: ",tuple[0] , " \tExpertise Area:",tuple[1]," \tWait = ",waitPeriod," seconds");
    daoFetchAndSaveUser(tuple[0], tuple[1]);
    ctr++;
    if (ctr < flatBatchInput.length) {
        setTimeout(fetchAndSaveBatchInput, waitPeriod);
    }
}

function main(){
    fetchAndSaveBatchInput();
    // TODO: Build users
    // TODO: Run the processor
}

main();

// Method to test validity of data received
function checkDataReceived(userScoreObject) {
    console.log("Data for ",userScoreObject.user_id);
    // Show questions:
    console.log("---: Questions :---");
    console.log(userScoreObject.expertise_area_list[0].question_list);
    console.log("---: Answers :---");
    console.log(userScoreObject.expertise_area_list[0].answer_list);
    console.log("---: Comments :---");
    console.log(userScoreObject.expertise_area_list[0].comment_list);
}

