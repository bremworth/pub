/**
 * Created by Udeeksh on 3/10/2015.
 * Update: Added mongoose methods to compile the schema and export a userModel
 */
    // TODO: Add bounty and favourite count
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var commentSchema = new Schema({
    id: Number,
    score: Number,
    parent_id: Number,
    parent_score: Number
});

var questionSchema = new Schema({
    id: Number,
    score: Number,
    view_count: Number,
    is_answered: Boolean,
    answer_count: Number,
    active_duration: Number
});

var answerSchema = new Schema({
    id: Number,
    score: Number,
    is_accepted: Boolean,
    parent_id: Number,
    parent_score: Number,
    parent_view_count: Number,
    parent_active_duration: Number
});

var userSchema = new Schema({
    user_id: Number,
    location: String,
    expertise_area_list:
        [{
        name: String,
        question_list:
            [questionSchema],
        answer_list:
            [answerSchema],
        comment_list:
            [commentSchema]
    }],
    badge_list:  [ {} ]
});



// Compilation
var userModel = mongoose.model('userModel',userSchema);
module.exports.userModel = userModel;
