/**
 * Created by Udeeksh on 2/10/2015.
 * -- 25.10.2015 Bug Fix
 *           -- Global variables retain the old values when module re-used; results are appended for dataset
 *           --- Solved by redeclaring all the required global vars inside the methods
 */

var restify = require('restify');
var service = restify.createJsonClient({
    url: 'https://api.stackexchange.com/2.2',
    version: '*'
});

/*
 Method description:
 * Step 1: Fetch all the answers from StackOverflow for a given user
 * Step 2: From the list of answers returned, extract questions Ids and create a new query string
 *      Step 2.1 : Fetch all the questions from and filter them with the tags
 *      Step 2.2 : Add the corresponding filtered answer Ids. These are the inherited attributes.
 *      Step 2.3 : Update the derived attributes in 'filteredAnswer' and create an array for all such objetcs
 * Step 3 : Update the question score object according to the question_score schema using the 'filteredAnswers' for inherited attributes
 */
// API call to fetch all the answers for a particular user
function getAns(queryString,expertiseArea,  callback) {

    // Bug-Fix: Re-Initialize global vars
    var Answer = function (){};
    var answerList = [];

    var parentPost = function(){};
    var parentPostList = [];

    var totalAnswers = 0;
    var answerIds = [];
    var quesAnsMap = {};
    var quesIdList = "";

    service.get(queryString, function (err, req, res, obj) {
        //TODO Check for error
        if (err){
            console.error("Error in fetching Answers for ", queryString);
            obj =[];
            return obj;
        }
        for (var itemKey in obj.items) {
            totalAnswers++;
            quesAnsMap[obj.items[itemKey].question_id] = [obj.items[itemKey].answer_id,itemKey];
            //ansIdItemKeyMap[obj.items[itemKey].answer_id] = itemKey;
        }
        // Filter the relevant answers based on their question tag
        quesIdList = Object.keys(quesAnsMap).join(';');
        var newQueryString = "/questions/" + quesIdList + "?order=desc&sort=votes&site=stackoverflow";
        // Second API call to fetch all the questions for the answers and filter as per tag
        service.get(newQueryString, function (err, req, res, obj_q) {
            // TODO: Error handling
            for (var itemKey in obj_q.items) {
                // Filter according to tags and update inherited attributes
                if (obj_q.items[itemKey].tags.indexOf(expertiseArea) != -1) {
                    answerIds.push(quesAnsMap[obj_q.items[itemKey].question_id]);
                    var parent = new parentPost();
                    parent['id'] = obj_q.items[itemKey].question_id;
                    parent['child_id'] = quesAnsMap[obj_q.items[itemKey].question_id][0];
                    parent['score'] = obj_q.items[itemKey].score;
                    parent['view_count'] = obj_q.items[itemKey].view_count;
                    parent['active_duration'] = (obj_q.items[itemKey].last_activity_date - obj_q.items[itemKey].creation_date);
                    // TODO: can get favourite count
                    parentPostList.push(parent);
                    //console.log(parent);
                }
            }
            for (var ctr in parentPostList){
                var parent = parentPostList[ctr];
                var ansKey = quesAnsMap[parent.id][1];
                var answer = new Answer();
                answer['id'] = obj.items[ansKey].answer_id;
                answer['score'] = obj.items[ansKey].score;
                answer['is_accepted'] = obj.items[ansKey].is_accepted;
                answer['parent_id'] = parent.id;
                answer['parent_score'] = parent.score;
                answer['parent_view_count'] = parent.view_count;
                answer['parent_active_duration'] = parent.active_duration;
                // TODO: get bounty won
                answerList.push(answer);
                //console.log("parent Id: ", parent.id, "child_key: ",ansKey);
                //console.log("Answer Object: \n",answer);
            }
            console.log("--Fetched all answers successfully.");
            return callback(answerList);
        });
    });
}

/*//Debug
 getAns(answerQueryString,expertiseArea, function(response){
 console.log("Finished!!!");
 });*/
module.exports.getAns = getAns;