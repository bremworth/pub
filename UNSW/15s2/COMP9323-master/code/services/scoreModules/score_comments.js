/**
 * Created by Udeeksh on 30/09/2015.
 */
/*Required Input : Hardcoded right now*/
var userId = "266795";
var expertiseArea = "node.js";

var restify = require('restify');
var service = restify.createJsonClient({
    url: 'https://api.stackexchange.com/2.2',
    version: '*'
});
//var commentQueryString = "/users/"+userId+"/comments?order=desc&max=999&sort=votes&site=stackoverflow";
var commentScore = {
    total_score: 0,
    post_score: 0
    //
};
var postCommMap = {};
var quesCommMap = {};
var ansCommMap = {};
var quesAnsMap ={};
var quesIdList = "";
var ansIdList = "";
var quesQueryString;
var ansQueryString;
var totalComments = 0;
quesIdList = "";

exports.getCommentsData = function(user_id) {
    service.get(commentQueryString, function(err, req, res, obj) {

        var commentQueryString = "/users/"+user_id+"/comments?order=desc&max=999&sort=votes&site=stackoverflow";
        
        for(var itemKey in obj.items){
            totalComments++;
            if (obj.items[itemKey].score >0) {
                postCommMap[obj.items[itemKey].post_id] = [obj.items[itemKey].comment_id, itemKey];
                //console.log("Post:Comm==> ",obj.items[itemKey].post_id," : ",obj.items[itemKey].comment_id);
            }
        }
        console.log("Total number of comments received for this API call: ",totalComments);
        console.log("Analyzing the comments");
        var postIdList = Object.keys(postCommMap).join(';');
        var postQueryString = "/posts/"+postIdList+"?order=desc&max=100&sort=votes&site=stackoverflow";
        //console.log(postQueryString);
        service.get(postQueryString, function(err,req,res,obj_p){
            // Determine if posts are questions or answers
            for (var itemKey in obj_p.items){
                if (obj_p.items[itemKey].post_type == "question") {
                    quesCommMap[obj_p.items[itemKey].post_id] = postCommMap[obj_p.items[itemKey].post_id];
                } else {
                    ansCommMap[obj_p.items[itemKey].post_id] = postCommMap[obj_p.items[itemKey].post_id];
                }
            }
            //console.log(quesCommMap);
            quesIdList = Object.keys(quesCommMap).join(';');
            quesQueryString = "/questions/"+quesIdList+"?order=desc&sort=votes&site=stackoverflow";
            //console.log("quesIds: ",quesQueryString);
            ansIdList = Object.keys(ansCommMap).join(';');
            ansQueryString = "/answers/"+ansIdList+"?order=desc&sort=votes&site=stackoverflow";
            //console.log("ansIds: ",ansQueryString);
            // Filter comments as per questions and answers
            service.get(quesQueryString, function(err,req,res,obj_q) {
            for (var itemKey in obj_q.items) {
                if (obj_q.items[itemKey].tags.indexOf(expertiseArea) != -1) {
                    // update the comment
                    var commKey = quesCommMap[obj_q.items[itemKey].question_id][1];
                    commentScore.total_score+= obj.items[commKey].score;
                    commentScore.post_score+= obj_q.items[itemKey].score;
                }
            }
                console.log("Score after analyzing comment->Post->Question\n",commentScore);
                // Filter the answers through their respective questions
                service.get(ansQueryString, function(err,req,res,obj_a) {
                    quesIdList = "";
                    for (var itemKey in obj_a.items) {
                        quesIdList+= (obj_a.items[itemKey].question_id + ";")
                        quesAnsMap[obj_a.items[itemKey].question_id] = [obj_a.items[itemKey].answer_id, itemKey];
                    }
                    quesQueryString = "/questions/"+quesIdList.slice(0,quesIdList.length-1)+"?order=desc&sort=votes&site=stackoverflow";
                    //console.log("Final API Call:",quesQueryString);
                    //console.log("Post: Comments = ",postCommMap);
                    // Final API call: Look for questions and their tags:
                    service.get(quesQueryString, function(err,req,res,obj_qq) {
                        for (var itemKey in obj_q.items) {
                            if (obj_qq.items[itemKey].tags.indexOf(expertiseArea) != -1) {
                                // update the comment
                                var ansKey = quesAnsMap[obj_qq.items[itemKey].question_id][1];
                                //TODO Possible Bug: Need to check further
                                var commKey = postCommMap[quesAnsMap[obj_qq.items[itemKey].question_id][0]];
                                //console.log(quesAnsMap[obj_qq.items[itemKey].question_id][0], commKey );
                                commentScore.post_score+= obj_a.items[ansKey].score;
                                commentScore.total_score+= obj.items[commKey[1]].score;
                            }
                        }
                        console.log("Score after analyzing comment->Post->Answer->Question\n",commentScore);
                    });
                });
            });
        });
    });
    
    return commentScore;
};
