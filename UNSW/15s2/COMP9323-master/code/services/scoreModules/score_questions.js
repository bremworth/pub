/**
 * Created by Udeeksh on 28/09/2015.
 */
/*Required Input : Hardcoded right now*/
var userId = "266795";
var expertiseArea = "node.js";

var restify = require('restify');
var service = restify.createJsonClient({
    url: 'https://api.stackexchange.com/2.2',
    version: '*'
});

//var questionQueryString = "/users/"+userId+"/questions?order=desc&max=999&sort=votes&site=stackoverflow";
// Question Score Schema
var questionScore = {
    total_questions: 0,
    answered_questions: 0,
    unanswered_questions: 0,
    total_answers: 0,
    total_score: 0,
    total_views: 0,
    total_activity_period: 0
};
var totalQues = 0;
var questionIds =  [];
/*
Method description:
 * Step 1: Fetch all the questions from StackOverflow for a given user
 * Step 2: Filter questions according to the tag category
 * Step 3 : Update the question score object according to the question_score schema
*/

// API call to StackOverflow to fetch all the users questions
exports.getQuestionData = function(user_id) {
    var questionQueryString = "/users/"+user_id+"/questions?order=desc&max=999&sort=votes&site=stackoverflow";

    service.get(questionQueryString, function (err, req, res, obj) {

        for (var itemKey in obj.items) {
            totalQues++;
            // filter on the basis of tags
            if (obj.items[itemKey].tags.indexOf(expertiseArea) != -1) {
                questionIds.push(obj.items[itemKey].question_id);
                // update score:
                questionScore.total_questions++;
                questionScore.total_answers += obj.items[itemKey].answer_count;
                questionScore.total_score += obj.items[itemKey].score;
                questionScore.total_views += obj.items[itemKey].view_count;
                questionScore.total_activity_period += (obj.items[itemKey].last_activity_date - obj.items[itemKey].creation_date);
                if (obj.items[itemKey].is_answered) {
                    questionScore.answered_questions++;
                } else {
                    questionScore.unanswered_questions++;
                }

            }
        }
        console.log("Total number of questions received for this API call: ", totalQues);
        console.log("Summarized Question Score specific to expertise Area: \n", questionScore);
    });
    return questionScore;
};