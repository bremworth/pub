/**
 * Created by Andrew Gosali on 1/10/2015.
 * 
 * Changes:
 *  Alvin Hielman - 11 Oct 2015 20:20 AEST
 *      + Implemented error first callback.
 *      + Code refactor for consistency.
 * 
 */

var express = require('express');
var restify = require('restify');
var dao = require('../model/dao');
var querystring = require('querystring');
var User = require('../model/schemas/user');


/**  Setup for StackOverflow authentication **/

// define CONSTANT values
const CLIENT_ID = '5857';
const CLIENT_SECRET = 'FIAG4s*VAqgdDzteTDz21Q((';
const KEY = 'TrnbM58HYFKx)mZPzvAsuQ((';
const HOST = 'https://stackexchange.com';
const PORT = '9001';
//const PORT = '8080';
//const WEB_URL = 'http://localhost:8080';
const WEB_URL = 'http://52.10.183.118:' +PORT;
const REDIRECT_URI = WEB_URL + '/callback';

// set up simple-oauth2
var oauth2 = require('simple-oauth2')({
    clientID: CLIENT_ID,
    clientSecret: CLIENT_SECRET,
    site: HOST,
    tokenPath: '/oauth/access_token',
    authorizationPath: '/oauth/'
});
var authURL = oauth2.authCode.authorizeURL({
    redirect_uri: REDIRECT_URI,
    scope: 'no_expiry'
    // add parameter here for https://stackexchange.com/oauth
});

// start express app
var app = express();

// prepare callback page
app.get('/callback', function (req, res) {
    res.send('use this code in the api call http://52.10.183.118:3000/account/connect/stackoverflow to get access token');
});

// start server at localhost
app.listen(PORT);


/**
 * getAuthURL - to get the authentication url from StackOverflow API
 * @param callback
 * @returns {*}
 */
exports.getAuthURL = function(callback) {
    return callback(null, authURL);
}

/**
 * linkProfile - this function is for controller to perform the linking profile module (authenticate SO, update db)
 * @param userid
 * @param SO_code
 * @param callback
 */
exports.linkProfile = function(userid, SO_code, callback) {
    getSOAccessToken(SO_code , function(err, access_token) {
        if(err)
            return callback(err);

        getSOUserId(access_token , function(SO_id) {
            dao.update(User,{user_id: userid},{SO_id: SO_id, SO_access_token: access_token}, function(docs) {
                if(docs)
                    return callback(null);

                var err = "Update data into database failed";
                return callback(err);
            });
        });
    });
}

/**
 * getSOAccessToken - process the code from auth url into access token
 * @param SO_code
 * @param callback
 */
function getSOAccessToken(SO_code, callback) {
    oauth2.authCode.getToken({
        code: SO_code,
        redirect_uri: REDIRECT_URI
    }, saveToken);

    function saveToken(error, result) {
        if (error) {
            return callback('StackOverflow Access Token Error', null);
        }
        if(!result){
            return callback('Token entered is invalid', null);
        }

        var token = oauth2.accessToken.create(result);
        var jsonObj = JSON.parse(JSON.stringify(token));
        var qs = querystring.parse(jsonObj.token.split("?")[0]);

        callback(null, qs.access_token);
    }
}

/**
 * getSOUSerId - to determine what SO_id that a access_token belongs to
 * @param access_token
 * @param callback
 */
function getSOUserId(access_token, callback) {
    var service = restify.createJsonClient({
        url: 'https://api.stackexchange.com/2.2',
        version: '*'
    });

    var path = "/me?site=stackoverflow&key="+KEY+"&access_token="+access_token;

    service.get(path, function (err, req, res, obj) {
        if (err) {
            console.log("API Call failed !",err.toString());
        }
        for (var itemKey in obj.items) {
            callback(obj.items[itemKey].user_id);
        }
    });
}
