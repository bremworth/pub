#version 120

varying vec3 N; 
varying vec3 v; 
varying vec3 normal, lightDir; 
varying vec2 texCoord; 

void main (void) {	
    //v = vec3(gl_ModelViewMatrix * gl_Vertex);
    //N = vec3(normalize(gl_NormalMatrix * normalize(gl_Normal)));
	//gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	
	vec4 ecPos; 
    ecPos = vec4(gl_ModelViewMatrix * gl_Vertex);
    v = vec3(gl_ModelViewMatrix * gl_Vertex);
    lightDir = normalize(vec3(gl_LightSource[0].position) - ecPos.xyz); 
    N = normalize(gl_NormalMatrix * gl_Normal); 
    texCoord = vec2(gl_MultiTexCoord0); 
    gl_Position = ftransform(); 
}

