package ass2.spec;


import java.io.File;
import java.io.FileNotFoundException;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLJPanel;
import javax.swing.JFrame;

import ass2.game.GameEngine;

import com.jogamp.opengl.util.FPSAnimator;
/*
 * Written by Alex Witting for COMP9415 October 2014
 * Press space to switch rendering modes (shaders) Both NPR and Phong+Textures are done for bonus marks
 * use the arrow keys to move the camera around. 
 */
public class Game extends JFrame  {

    private Terrain terrain;

    public Game(Terrain terrain) {
    	super("Assignment 2");
    	this.terrain = terrain;
    }

    public void run() {
    	  GLProfile glp = GLProfile.getDefault();
          GLCapabilities caps = new GLCapabilities(glp);
          GLJPanel panel = new GLJPanel(caps);

          GameEngine engine = new GameEngine(terrain); 
          
          panel.addGLEventListener(engine);
          panel.addMouseMotionListener(engine.getCamera());
          panel.addKeyListener(engine.getCamera());
          
          FPSAnimator animator = new FPSAnimator(10);
          animator.add(panel);
          animator.start();

          getContentPane().add(panel);
          setSize(800, 600);        
          setVisible(true);
          setDefaultCloseOperation(EXIT_ON_CLOSE);        
    }
    
    /**
     * Load a level file and display it.
     * 
     * @param args - The first argument is a level file in JSON format
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
    	Terrain terrain = LevelIO.load(new File(args[0])); //TODO - before submit!
        //Terrain terrain = LevelIO.load(new File("test1.json"));
        Game game = new Game(terrain);
        game.run();
    }


}
