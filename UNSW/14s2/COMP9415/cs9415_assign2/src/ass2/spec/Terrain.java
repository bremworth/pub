package ass2.spec;

import java.awt.Dimension;

import java.util.ArrayList;
import java.util.List;



public class Terrain {

    private Dimension mySize;
    private double[][] myAltitude;
    private List<Tree> myTrees;
    private List<Road> myRoads;
    private float[] mySunlight;

    /**
     * Create a new terrain
     *
     * @param width The number of vertices in the x-direction
     * @param depth The number of vertices in the z-direction
     */
    public Terrain(int width, int depth) {
        mySize = new Dimension(width, depth);
        myAltitude = new double[width][depth];
        myTrees = new ArrayList<Tree>();
        myRoads = new ArrayList<Road>();
        mySunlight = new float[3];
    }
    
    public Terrain(Dimension size) {
        this(size.width, size.height);
    }

    public Dimension size() {
        return mySize;
    }

    public List<Tree> trees() {
        return myTrees;
    }

    public List<Road> roads() {
        return myRoads;
    }

    public float[] getSunlight() {
        return mySunlight;
    }

    /**
     * Set the sunlight direction. 
     * 
     * Note: the sun should be treated as a directional light, without a position
     * 
     * @param dx
     * @param dy
     * @param dz
     */
    public void setSunlightDir(float dx, float dy, float dz) {
        mySunlight[0] = dx;
        mySunlight[1] = dy;
        mySunlight[2] = dz;        
    }
    
    /**
     * Resize the terrain, copying any old altitudes. 
     * 
     * @param width
     * @param height
     */
    public void setSize(int width, int height) {
        mySize = new Dimension(width, height);
        double[][] oldAlt = myAltitude;
        myAltitude = new double[width][height];
        
        for (int i = 0; i < width && i < oldAlt.length; i++) {
            for (int j = 0; j < height && j < oldAlt[i].length; j++) {
                myAltitude[i][j] = oldAlt[i][j];
            }
        }
    }

    /**
     * Get the altitude at a grid point
     * 
     * @param x
     * @param z
     * @return
     */
    public double getGridAltitude(int x, int z) {
        return myAltitude[x][z];
    }

    /**
     * Set the altitude at a grid point
     * 
     * @param x
     * @param z
     * @return
     */
    public void setGridAltitude(int x, int z, double h) {
        myAltitude[x][z] = h;
    }

    /**
     * Get the altitude at an arbitrary point. 
     * Non-integer points should be interpolated from neighbouring grid points
     *  
     * @param x
     * @param z
     * @return
     */
    public double altitude(double x, double z) {
    	
        double altitude = 0;
        //
        try {
	/*        if ((x % 1 == 0) && (z % 1 == 0)) {
	        	return getGridAltitude((int)x,(int)z);
	        }
	*/        
        	
	        int x1 = (int) Math.floor(x);
	    	int z1 = (int) Math.floor(z);
	    	
	    	int x2 = (int) Math.floor(x);
	    	int z2 = (int) Math.ceil(z);
	    	
	    	int x3 = (int) Math.ceil(x);
	    	int z3 = (int) Math.ceil(z);
	    	
	    	int x4 = (int) Math.ceil(x);
	    	int z4 = (int) Math.floor(z);
	    	//c00
	    	double y1 = getGridAltitude(x1,z1);
	    	//c10
	        double y2 = getGridAltitude(x2,z2);
	        //c01
	        double y3 = getGridAltitude(x3,z3);
	        //c11
	        double y4 = getGridAltitude(x4,z4);
	        
	        //lerp
	        double tx = x-x1;
	        double tz = z-z1;
	        altitude = y1 * (1 - tx) * (1 - tz) + y4 * tx * (1 - tz) + y2 * (1 - tx) * tz + y3 * tx * tz;
	        return altitude;
        } catch (Exception e) {
        	//System.out.println("Out of grid: "+x+" "+z);
        	return -1; //return lazy
        }
        
    }
    
    public double lerp(double P, double Q, double t) {
    	//lerp(P,Q,t) = P(1-t) + tQ
    	return (P*(1-t)+Q*t);
    }

    /**
     * Add a tree at the specified (x,z) point. 
     * The tree's y coordinate is calculated from the altitude of the terrain at that point.
     * 
     * @param x
     * @param z
     */
    public void addTree(double x, double z) {
        double y = altitude(x, z);
        Tree tree = new Tree(x, y, z);
        myTrees.add(tree);
    }


    /**
     * Add a road. 
     * 
     * @param x
     * @param z
     */
    public void addRoad(double width, double[] spine) {
        Road road = new Road(width, spine);
        myRoads.add(road);        
    }


}
