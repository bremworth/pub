package ass2.objects;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import com.jogamp.opengl.util.gl2.GLUT;

import ass2.render.Material;
import ass2.render.Shader;




public class PlayerModel extends Model {

    public PlayerModel(GameObject parent, Material mat) {
		super(parent, mat);
	}


	@Override
    public void drawModel(GL2 gl) {
        // Draw the model
        GLUT glut = new GLUT();
        gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2.GL_FILL);

        gl.glFrontFace(GL2.GL_CW);
        glut.glutSolidTeapot(1);
        gl.glFrontFace(GL2.GL_CCW);
    }
}
