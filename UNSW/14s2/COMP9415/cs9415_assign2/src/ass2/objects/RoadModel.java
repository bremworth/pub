package ass2.objects;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.media.opengl.GL2;

import ass2.game.MathUtil;
import ass2.game.Point;
import ass2.render.Material;
import ass2.render.Shader;
import ass2.spec.Road;
import ass2.spec.Terrain;


public class RoadModel extends Model {
    private Road road;
    private ArrayList<Point> pointsList = new ArrayList<Point>();
    
    public RoadModel(GameObject parent, Road road, Material mat) {
        super(parent, mat);
        this.road = road;
        this.pointsList = road.getPoints();
    }
 
    @Override
    public void drawModel(GL2 gl) {


    	gl.glBegin(GL2.GL_QUADS);
    	{
    		// The first polygon is the setup
			Point p0 = pointsList.get(0);   //0,1
			Point p1 = pointsList.get(1);   //1,1 - 
		
			//getNormal
			double dx = p1.x - p0.x; //delta x
			double dy = p1.y - p0.y; //delta y
			double dz = p1.z - p0.z; //delta z
			//normalise
			double linelength = Math.sqrt(dx * dx + dy * dy + dz * dz); 
			dx /= linelength;
			dy /= linelength;
			dz /= linelength;

			//normal with length width * 0.5
			double px = 0.5 * road.width() * dz; 
			double pz = 0.5 * road.width() * dx;
			
			double [] n = {0,1,0}; //flat road

			gl.glNormal3dv(n,0);
			gl.glTexCoord2d(0,1);
			gl.glVertex3d(p0.x - px, 0.01, p0.z + pz);
			gl.glTexCoord2d(1,1);
			gl.glVertex3d(p1.x - px, 0.01, p1.z + pz);
			gl.glTexCoord2d(1,0);
			gl.glVertex3d(p1.x + px, 0.01, p1.z - pz);
			gl.glTexCoord2d(0,0);
			gl.glVertex3d(p0.x + px, 0.01, p0.z - pz);
			
			double prevX = p1.x + px;
			double prevZ = p1.z - pz;
			double prev1X = p1.x - px;
			double prev1Z = p1.z + pz;
		//Build the rest of the road
   		for(int i = 1; i < pointsList.size()-1; ++i) {	
    			p0 = pointsList.get(i);   //0,1
    			p1 = pointsList.get(i+1);   //1,1
    			//getNormal
    			dx = p1.x - p0.x; //delta x
    			dy = p1.y - p0.y; //delta y
    			dz = p1.z - p0.z; //delta z
    			//normalise
    			linelength = Math.sqrt(dx * dx + +dy* dy + dz * dz); 
    			dx /= linelength;
    			dy /= linelength;
    			dz /= linelength;
  
    			//perpendicular vector with length width /2
    			px = 0.5 * road.width() * dz; 
    			pz = 0.5 * road.width() * dx;

    			
    	    	gl.glNormal3dv(n,0);
    			gl.glTexCoord2d(0,1);
    			gl.glVertex3d(prev1X, 0.01, prev1Z); //lift the road 0.01 above the ground
    			gl.glTexCoord2d(1,1);
    			gl.glVertex3d(p1.x - px, 0.01, p1.z + pz);
    			gl.glTexCoord2d(1,0);
    			gl.glVertex3d(p1.x + px, 0.01, p1.z - pz);
    			gl.glTexCoord2d(0,0);
    			gl.glVertex3d(prevX, 0.01, prevZ);
    			//next polygon shares the previous edge points of the last poly
    			prevX = p1.x + px;
    			prevZ = p1.z - pz;
    			prev1X = p1.x - px;
    			prev1Z = p1.z + pz;
    		}

    	}
        gl.glEnd();
     
    }
}
