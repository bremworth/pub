package ass2.objects;


import javax.media.opengl.GL2;

import ass2.game.MathUtil;
import ass2.render.Material;
import ass2.render.Shader;
import ass2.spec.Terrain;

public class Model extends GameObject {

    private Material mat;
    private boolean smoothShading = false;

    
    public Model(GameObject parent, Material mat) {
        super(parent, "Model");
        this.mat = mat;
    }
    
    public void init(GL2 gl) {
    	gl.glShadeModel(smoothShading ? GL2.GL_SMOOTH : GL2.GL_FLAT); 
	    gl.glBindTexture(GL2.GL_TEXTURE_2D, mat.getTex().getTextureId());
		gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_MODULATE);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, mat.getShader().getAmbient(), 0);
        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, mat.getShader().getDiffuse(), 0);
        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, mat.getShader().getSpecular(), 0);
        gl.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, mat.getShader().getShininess());
    }
    
    @Override
    public void drawSelf(GL2 gl) {
    	init(gl);
    	drawModel(gl);
    }
    
    public void drawModel(GL2 gl) {
    }
    
    public void setMaterial(GL2 gl, Material material) {
    	gl.glBindTexture(GL2.GL_TEXTURE_2D, material.getTex().getTextureId());
		gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_MODULATE);
		
        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, material.getShader().getAmbient(), 0);
        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, material.getShader().getDiffuse(), 0);
        gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, material.getShader().getSpecular(), 0);
        gl.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, material.getShader().getShininess());  
 
    }
    
	public boolean isSmooth() {
		return smoothShading;
	}

	public void setSmooth(boolean smoothShading) {
		this.smoothShading = smoothShading;
	}

}
