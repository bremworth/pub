package ass2.objects;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import com.jogamp.opengl.util.gl2.GLUT;

import ass2.render.Material;
import ass2.render.Shader;




public class TeapotModel extends Model {

    public TeapotModel(GameObject parent, Material mat) {
		super(parent, mat);
	}

	@Override
    public void drawModel(GL2 gl) {
        // Draw the model
        GLUT glut = new GLUT();
        gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL2.GL_FILL);
        
        //get teapot
        // the builtin teapot is back-to-front
        // https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man3/glutSolidTeapot.3.html
        gl.glFrontFace(GL2.GL_CW);
        glut.glutSolidTeapot(1);
        gl.glFrontFace(GL2.GL_CCW);
    }
}
