package ass2.objects;

import java.util.ArrayList;
import java.util.List;

import javax.media.opengl.GL2;

import ass2.game.MathUtil;

public class GameObject {

    // the list of all GameObjects in the scene tree
    public final static List<GameObject> ALL_OBJECTS = new ArrayList<GameObject>();
    
    // the root of the scene tree
    public final static GameObject ROOT = new GameObject();
    
    // the links in the scene tree
    private GameObject myParent;
    private List<GameObject> myChildren;

    // the local transformation
    //myRotation should be normalised to the range (-180..180)
    private double myRotation;
    private double myScale;
    private double[] myTranslation;
    private String objectType;
    
    // is this part of the tree showing?
    private boolean amShowing;

    private GameObject() {
        myParent = null;
        myChildren = new ArrayList<GameObject>();

        myRotation = 0;
        myScale = 1;
        myTranslation = new double[3];
        myTranslation[0] = 0;
        myTranslation[1] = 0;
        myTranslation[2] = 0;

        amShowing = true;
        
        ALL_OBJECTS.add(this);
    }

    public GameObject(GameObject parent, String type) {
        myParent = parent;
        objectType = type;
        myChildren = new ArrayList<GameObject>();

        parent.myChildren.add(this);

        myRotation = 0;
        myScale = 1;
        myTranslation = new double[3]; 
        myTranslation[0] = 0;
        myTranslation[1] = 0;
        myTranslation[2] = 0;

        // initially showing
        amShowing = true;

        ALL_OBJECTS.add(this);
    }


    public void destroy() {
        for (GameObject child : myChildren) {
            child.destroy();
        }
        
        myParent.myChildren.remove(this);
        ALL_OBJECTS.remove(this);
    }
    

    
    /**
     * Get the parent of this game object
     * 
     * @return
     */
    public GameObject getParent() {
        return myParent;
    }

    /**
     * Get the children of this object
     * 
     * @return
     */
    public List<GameObject> getChildren() {
        return myChildren;
    }

    /**
     * Get the local rotation (in degrees)
     * 
     * @return
     */
    public double getRotation() {
        return myRotation;
    }

    /**
     * Set the local rotation (in degrees)
     * 
     * @return
     */
    public void setRotation(double rotation) {
        myRotation = MathUtil.normaliseAngle(rotation);
    }

    /**
     * Rotate the object by the given angle (in degrees)
     * 
     * @param angle
     */
    public void rotate(double angle) {
        myRotation += angle;
        myRotation = MathUtil.normaliseAngle(myRotation);
    }

    /**
     * Get the local scale
     * 
     * @return
     */
    public double getScale() {
        return myScale;
    }

    /**
     * Set the local scale
     * 
     * @param scale
     */
    public void setScale(double scale) {
        myScale = scale;
    }

    /**
     * Multiply the scale of the object by the given factor
     * 
     * @param factor
     */
    public void scale(double factor) {
        myScale *= factor;
    }

    /**
     * Get the local position of the object 
     * 
     * @return
     */
    public double[] getPosition() {
        return myTranslation;
    }

    /**
     * Set the local position of the object
     * 
     * @param x
     * @param y
     */
    public void setPosition(double x, double y, double z) {
        myTranslation[0] = x;
        myTranslation[1] = y;
        myTranslation[2] = z;
    }

    /**
     * Set the local position of the object
     * 
     * @param x
     * @param y
     */
    public void setPosition(double []pos) {
        myTranslation = pos;
    }
    
    public void setPosition(float []pos) {
        myTranslation[0] = pos[0];
        myTranslation[1] = pos[1];
        myTranslation[2] = pos[2];
    }
    
    
    /**
     * Move the object by the specified offset in local coordinates
     * 
     * @param dx
     * @param dy
     */
    public void translate(double dx, double dy, double dz) {
        myTranslation[0] += dx;
        myTranslation[1] += dy;
        myTranslation[2] += dz;
    }
    
    public void translate(double d[]) {
        myTranslation[0] += d[0];
        myTranslation[1] += d[1];
        myTranslation[2] += d[2];
    }

    /**
     * Test if the object is visible
     * 
     * @return
     */
    public boolean isShowing() {
        return amShowing;
    }

    /**
     * Set the showing flag to make the object visible (true) or invisible (false).
     * This flag should also apply to all descendents of this object.
     * 
     * @param showing
     */
    public void show(boolean showing) {
        amShowing = showing;
    }

    /**
     * Update the object. This method is called once per frame. 
     *      * 
     * @param dt The amount of time since the last update (in seconds)
     */
    public void update(double dt) {
        
    }

    /**
     * Draw the object (but not any descendants)
     *      * 
     * @param gl
     */
    public void drawSelf(GL2 gl) {
        // do nothing
    }

    /**
     * Draw the object and all of its descendants recursively.
     * 
     * 
     * @param gl
     */
   
    public void draw(GL2 gl) {
        // don't draw if it is not showing
        if (!amShowing) {
            return;
        }

	   gl.glPushMatrix();
		   {
		   //translate the coordinate frame to put the overall object into position         	
		   gl.glTranslated(myTranslation[0], myTranslation[1], myTranslation[2]);
		   gl.glRotated(0, myRotation, 0, 1.0f);
		   gl.glScaled(myScale, myScale, myScale);
		   //Call drawSelf() to draw the object itself
		   drawSelf(gl);
		   
		   for(GameObject child : myChildren) {
		       child.draw(gl);
		   }
	   }
	   gl.glPopMatrix();
        
    }
 

    public double[] getGlobalPosition() {
        
        if(myParent == null) {
        	return myTranslation;
        }
        double[][] translate = MathUtil.translationMatrix(myTranslation);
        double[][] rotate = MathUtil.rotationMatrix(myRotation);
        double[][] scale = MathUtil.scaleMatrix(myScale);
  
        double[][] global_position;
        global_position = MathUtil.multiply(translate,rotate);
        global_position = MathUtil.multiply(global_position,scale);
	    global_position = MathUtil.multiply(myParent.getParentGlobal(), global_position);

        double[] position = new double[2];
        position[0] = global_position[0][2];
        position[1] = global_position[1][2];
        
        return position;
    }
    
    public double[][] getParentGlobal() {
    	double[][] translate = MathUtil.translationMatrix(myTranslation);
        double[][] rotate = MathUtil.rotationMatrix(myRotation);
        double[][] scale = MathUtil.scaleMatrix(myScale);
  
        double[][] local_position;
        local_position = MathUtil.multiply(translate,rotate);
        local_position = MathUtil.multiply(local_position,scale);
        
    	if(myParent == null) {
        	return local_position;
        }
 	
        return  MathUtil.multiply(myParent.getParentGlobal(), local_position);
    }


    public double getGlobalRotation() {

    	double angle = myRotation;

    	GameObject parent = myParent;
        
    	while(parent != null) {
    		angle = MathUtil.normaliseAngle(parent.getRotation()+angle);
    		parent = parent.myParent;
    	}
    	
        return angle;

    }

    public double getGlobalScale() {
    	
    	double scale = myScale;

    	GameObject parent = myParent;
        
    	while(parent != null) {
    		scale *= parent.getScale();
    		parent = parent.myParent;
    	}
    	
    	return scale;

    }

    public void setParent(GameObject parent) {
    	//Get the objects old global positions
    	double[] OldObjectGlobalTranslation = getGlobalPosition(); 
        double OldObjectGlobalRotation = getGlobalRotation(); 
        double OldObjectGlobalScale = getGlobalScale(); 
        //Do the switch
        myParent.myChildren.remove(this);
        myParent = parent;
        myParent.myChildren.add(this);
        
        //get the new parents global position
        double ParentGlobalScale = myParent.getGlobalScale();
        double ParentGlobalRotation = myParent.getGlobalRotation(); 
        double[] ParentGlobalTranslation = myParent.getGlobalPosition(); 
        
        ParentGlobalTranslation[0] = -ParentGlobalTranslation[0];
        ParentGlobalTranslation[1] = -ParentGlobalTranslation[1];
        double[][] inverse_scale= MathUtil.scaleMatrix(1.0/ParentGlobalScale);;
        double[][] inverse_rotation= MathUtil.rotationMatrix(-ParentGlobalRotation);;
        double[][] inverse_translation = MathUtil.translationMatrix(ParentGlobalTranslation);
        
        double[][] parent_inverse = MathUtil.multiply(MathUtil.multiply(inverse_scale, inverse_rotation), inverse_translation);
        
        double[] phi = new double[3];
        phi[0] = OldObjectGlobalTranslation[0];
        phi[1] = OldObjectGlobalTranslation[1];
        phi[2] = 1;
        
        //Apply the new position
        myTranslation = MathUtil.multiply(parent_inverse, phi);
        myRotation = MathUtil.normaliseAngle(OldObjectGlobalRotation - myParent.getGlobalRotation());
        myScale = OldObjectGlobalScale / myParent.getGlobalScale();
 
    }

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
    
    
}

