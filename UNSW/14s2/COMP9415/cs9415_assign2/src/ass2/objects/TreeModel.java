package ass2.objects;

import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

import com.jogamp.opengl.util.gl2.GLUT;

import ass2.render.Material;
import ass2.render.Shader;


public class TreeModel extends Model {
	private Material eyes;
	private Material nose;
	
    public TreeModel(GameObject parent, Material mat, Material mat2, Material mat3) {
        super(parent, mat);
        this.eyes = mat2;
        this.nose = mat3;
    }


    @Override
    public void drawModel(GL2 gl) {
    	GLU glu = new GLU();
    	 GLUquadric quadric =  glu.gluNewQuadric();
    	 glu.gluQuadricTexture(quadric, true);

    	//snowman
	    // Draw Body
    	 gl.glTranslatef(0.0f ,0.60f, 0.0f);
  		glu.gluSphere(quadric, 0.60f, 20, 20);
    	gl.glTranslatef(0.0f ,0.8f, 0.0f);
    	glu.gluSphere(quadric, 0.40f, 20, 20);
    	// Draw Head
    	gl.glTranslatef(0.0f, 0.6f, 0.0f);
    	glu.gluSphere(quadric, 0.25f, 20, 20);
    	// Draw Eyes
    	this.setMaterial(gl, eyes);
    	gl.glColor3f(0.0f, 0.0f, 0.0f);
        gl.glTranslatef(0.05f, 0.10f, 0.18f);
        glu.gluSphere(quadric, 0.05f, 10, 10);
    	gl.glTranslatef(-0.1f, 0.0f, 0.0f);
    	glu.gluSphere(quadric, 0.05f, 10, 10);
    	//Draw Nose
    	this.setMaterial(gl, nose);
    	gl.glColor3f(1.0f, 0.0f, 0.0f);
    	gl.glTranslatef(0.05f, -0.1f, -0.1f);
    	glu.gluCylinder(quadric, 0.08f, 0.01f, 0.4f, 10, 2);
    }
   
}
