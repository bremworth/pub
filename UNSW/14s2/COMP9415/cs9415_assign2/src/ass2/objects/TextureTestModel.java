package ass2.objects;


import javax.media.opengl.GL2;

import ass2.game.MathUtil;
import ass2.render.Material;
import ass2.render.Shader;
import ass2.spec.Terrain;

public class TextureTestModel extends Model {

    private Terrain t;

    
    public TextureTestModel(GameObject parent, Terrain w, Material mat) {
        super(parent, mat);
        this.t = w;
    }


    @Override
    public void drawModel(GL2 gl) {        
 
	    gl.glBegin(GL2.GL_TRIANGLES);
	    {

	    	for (int z = 0 ; z < 10; ++z) {
	    		for (int x = 0 ; x < 10; ++x) {
	    			double y = -1.0;
	    			//y = t.getGridAltitude(x,z);
		    		double p0[] = {x,y,z};
		    		//y = t.getGridAltitude(x,z+1);
		    		double p1[] = {x,y, z+1.0};
		    		//y = t.getGridAltitude(x+1,z+1);
		    		double p2[] = {x+1.0,y, z+1.0};
		    		//y = t.getGridAltitude(x+1,z);
		    		double p3[] = {x+1.0,y, z};
		    		double [] n1 = MathUtil.getNormal(p0,p1,p2);
		    		//second triangle
		    		double [] n2 = MathUtil.getNormal(p0,p2,p3);
	
		    		
		    		//First triangle
			    	gl.glNormal3dv(n1,0);
			    	gl.glTexCoord2d(0,0);
			    	gl.glVertex3dv(p0,0);
			    	gl.glTexCoord2d(0,1);
		    		gl.glVertex3dv(p1,0);   	
		    		gl.glTexCoord2d(1,0);
		    		gl.glVertex3dv(p2,0);
		    		
		    		//Second triangle
			    	gl.glNormal3dv(n2,0);
			    	gl.glTexCoord2d(0,0);
			    	gl.glVertex3dv(p0,0);
			    	gl.glTexCoord2d(1,1);
			    	gl.glVertex3dv(p2,0);
			    	gl.glTexCoord2d(0,1);

			    	gl.glVertex3dv(p3,0);
			 
	    		}
	    	}
	    	
	    }
	    gl.glEnd();
	    	
    }

}
