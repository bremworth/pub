package ass2.render;

public class CompilationException extends RuntimeException {

    public CompilationException(String message) {
        super(message);
    }
    
}