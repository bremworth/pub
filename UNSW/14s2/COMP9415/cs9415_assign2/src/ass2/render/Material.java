package ass2.render;

import java.io.File;
import java.io.IOException;

import javax.media.opengl.GL2;

public class Material {
	private Shader shader;
	private Shader vertexShader;
	private Shader fragmentShader;
	private Texture texture;
	private String name;
	
	public Material() {
		
	}
	
	public Material(String name, Shader shader, Texture tex) {
		this.name = name;
		this.shader = shader;
		this.texture = tex;
		
	}
	
	public Material(Shader shader, Shader vertexShader,
			Shader fragmentShader, Texture texture, String name) {
		super();
		this.shader = shader;
		this.vertexShader = vertexShader;
		this.fragmentShader = fragmentShader;
		this.texture = texture;
		this.name = name;
	}

	public void setGLSL(GL2 gl, String name) {
		String fragfile = name+"Fragment.glsl"; //Naming convention of NAMEFragment.glsl or NAMEVertex.glsl
		String vertfile = name+"Vertex.glsl";
		try {
			vertexShader = new Shader(GL2.GL_VERTEX_SHADER, new File(vertfile));
			fragmentShader = new Shader(GL2.GL_FRAGMENT_SHADER, new File(fragfile));
			vertexShader.compile(gl);
			fragmentShader.compile(gl);
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        
	}
	
	public int getFragID() {
		return this.fragmentShader.getID();
	}
	
	public int getVertID() {
		return this.vertexShader.getID();
	}
	
	public Shader getShader() {
		return shader;
	}

	public void setShader(Shader shader) {
		this.shader = shader;
	}

	public Shader getVertexShader() {
		return vertexShader;
	}

	public void setVertexShader(Shader vertexShader) {
		this.vertexShader = vertexShader;
	}

	public Shader getFragmentShader() {
		return fragmentShader;
	}

	public void setFragmentShader(Shader fragmentShader) {
		this.fragmentShader = fragmentShader;
	}

	public Texture getTexture() {
		return texture;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Texture getTex() {
    	return texture;
    }
    public void setTex(Texture tex) {
    	this.texture = tex;
    }
	

}
