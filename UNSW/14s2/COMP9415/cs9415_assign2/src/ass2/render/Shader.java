package ass2.render;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;

import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;



public class Shader {

    private int phong;
    private boolean modulate;
    private boolean useSpecular;
    private String name;
    private float[] ambient = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
    private float[] diffuse = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
    private float[] specular = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
    private int shininess = 50;
    private String[] mySource;
    private int myType;
    private int myID;
    

    public Shader(String name) {
    	this.name = name;
    }
  

    //read file into a string
    public Shader(int type, File sourceFile) throws IOException {
    	myType = type;
    	try {
    		BufferedReader reader = new BufferedReader(new InputStreamReader( new FileInputStream(sourceFile)));
    		StringWriter writer = new StringWriter();
    		mySource = new String[1];
    		String line = reader.readLine();
    		while (line != null) {
    			writer.write(line);
    			writer.write("\n");
    			line = reader.readLine();
    		}
    		reader.close();
    		mySource[0] = writer.getBuffer().toString();
    	} catch (IOException e) {
    		throw new RuntimeException(e);
    	}
    }
    
    public int getID() {
        return myID;
    }
    
    public void printShader(){
    	int i =0;
    	for(i=0; i < mySource.length;i++){
    		System.out.println(mySource[i]);
    	}
    }

    public void compile(GL2 gl) {
    	

    	printShader();
        myID = gl.glCreateShader(myType);
        gl.glShaderSource(myID, 1, mySource,
        new int[] { mySource[0].length() }, 0);
        gl.glCompileShader(myID);
        
        //Check compile status.
        int[] compiled = new int[1];
        gl.glGetShaderiv(myID, GL2ES2.GL_COMPILE_STATUS, compiled, 0);
        if (compiled[0] == 0) {
            int[] logLength = new int[1];
            gl.glGetShaderiv(myID, GL2ES2.GL_INFO_LOG_LENGTH, logLength, 0);

            byte[] log = new byte[logLength[0]];
            gl.glGetShaderInfoLog(myID, logLength[0], (int[]) null, 0, log, 0);

            throw new CompilationException("Error compiling the shader: "
                    + new String(log));
        }
    }
    

	public boolean isUseSpecular() {
		return useSpecular;
	}

	public void setUseSpecular(boolean useSpecular) {
		this.useSpecular = useSpecular;
	}
	
	public int getShininess() {
		return shininess;
	}

	public void setShininess(int shininess) {
		this.shininess = shininess;
	}

	public float[] getAmbient() {
		return ambient;
	}
	public void setAmbient(float[] ambient) {
		this.ambient = ambient;
	}
	public float[] getDiffuse() {
		return diffuse;
	}
	public void setDiffuse(float[] diffuse) {
		this.diffuse = diffuse;
	}
	public float[] getSpecular() {
		return specular;
	}
	public void setSpecular(float[] specular) {
		this.specular = specular;
	}

	public int getPhong() {
		return phong;
	}
	public void setPhong(int phong) {
		this.phong = phong;
	}
	public boolean isModulate() {
		return modulate;
	}
	public void setModulate(boolean modulate) {
		this.modulate = modulate;
	}

}
