package ass2.render;

import javax.media.opengl.GL2;

import ass2.objects.GameObject;


public class Light extends GameObject {
	  
		private float ambient[] = new float[4];
	    private float diffuse[]= new float[4];
	    private float specular[] = new float[4];
	    private boolean modulate;
	    private boolean useSpecular;
	    private float intensity = 0.1f;
	    
	    
	    public Light(GameObject parent) {
			super(parent, "Light");
			this.ambient[0] = this.ambient[1] = this.ambient[2] = 5*intensity;
			this.ambient[3] = 1.0f;
			this.diffuse[0] = this.diffuse[1] = this.diffuse[2] = 5*intensity;
			this.diffuse[3] = 1.0f;
			this.specular[0] = this.specular[1] = this.specular[2] = 5*intensity;
			this.specular[3] = 1.0f;
			this.modulate = true;
			this.useSpecular = true;
		}
	    
		public Light(GameObject parent,float ambient, float diffuse, float specular,
			 boolean mod, boolean spec, float intensity) {
			super(parent, "Light");
			this.intensity = intensity;
			this.ambient[0] = this.ambient[1] = this.ambient[2] = ambient*intensity;
			this.ambient[3] = 0.1f;
			this.diffuse[0] = this.diffuse[1] = this.diffuse[2] = diffuse*intensity;
			this.diffuse[3] = 0.1f;
			this.specular[0] = this.specular[1] = this.specular[2] = specular*intensity;
			this.specular[3] = 0.1f;
			this.modulate = mod;
			this.useSpecular = spec;
		}
		@Override
	    public void drawSelf(GL2 gl) {
				double p[] = getPosition();
				
		        float[] pos = new float[] { (float) p[0], (float) p[1], (float) p[2], 1.0f }; //FIXME 
		        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, pos, 0);
		        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, ambient, 0);
		        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, diffuse, 0);
		        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, specular, 0);
		        
		        if(isUseSpecular()){
		        	gl.glLightModeli(GL2.GL_LIGHT_MODEL_COLOR_CONTROL, GL2.GL_SEPARATE_SPECULAR_COLOR);
		        }else {
		        	gl.glLightModeli(GL2.GL_LIGHT_MODEL_COLOR_CONTROL, GL2.GL_SINGLE_COLOR);
		        }
		}


		public boolean isUseSpecular() {
			return useSpecular;
		}

		public void setUseSpecular(boolean useSpecular) {
			this.useSpecular = useSpecular;
		}

		public boolean isModulate() {
			return modulate;
		}
		public void setModulate(boolean modulate) {
			this.modulate = modulate;
		}

		public float[] getAmbient() {
			return ambient;
		}

		public void setAmbient(float[] ambient) {
			this.ambient = ambient;
		}
	
		public void setAmbient(float ambient) {
			this.ambient[0] = this.ambient[1] = this.ambient[2] = ambient*intensity;
			this.ambient[3] = 0.1f;
		}

		public float[] getDiffuse() {
			return diffuse;
		}

		public void setDiffuse(float[] diffuse) {
			this.diffuse = diffuse;
		}
		public void setDiffuse(float diffuse) {
			this.diffuse[0] = this.diffuse[1] = this.diffuse[2] = diffuse*intensity;
			this.diffuse[3] = 0.1f;
		}
		public float[] getSpecular() {
			return specular;
		}

		public void setSpecular(float[] specular) {
			this.specular = specular;
		}
		public void setSpecular(float specular) {
			this.specular[0] = this.specular[1] = this.specular[2] = specular*intensity;
			this.specular[3] = 0.1f;
		}
		public float getIntensity() {
			return intensity;
		}

		public void setIntensity(float intensity) {
			this.intensity = intensity;
		}
		
		
}
