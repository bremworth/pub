package ass2.game;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;

import ass2.objects.GameObject;
import ass2.objects.GroundModel;
import ass2.objects.PlayerModel;
import ass2.objects.RoadModel;
import ass2.objects.TeapotModel;
import ass2.objects.TextureTestModel;
import ass2.objects.TreeModel;
import ass2.render.CompilationException;
import ass2.render.Light;
import ass2.render.Material;
import ass2.render.Shader;
import ass2.render.Texture;
import ass2.spec.Road;
import ass2.spec.Terrain;
import ass2.spec.Tree;

/**
 * 
 * Press space to switch rendering modes (shaders) Both NPR and Phong+Textures are done for bonus marks
 * use the arrow keys to move the camera around. 
 */
public class GameEngine implements GLEventListener {
	private TeapotModel testObj;
	private PlayerModel player;
    private CameraView camera;
    private long myTime;
    private Terrain world;
    private Light sun;
    private boolean nightmode = true;
    private static final boolean DEBUG = false;

    //Materials/Shaders/Textures
	private Shading shading = Shading.PHONG_TEXTURE; 
	private ArrayList<Integer> shaderPrograms = new ArrayList<Integer>();
	private HashMap<String,Material> materialList = new HashMap<String,Material>();
	private int npr_shaderprogram;
	private int npr2_shaderprogram;
	private int phong_shaderprogram;
    
    
	public enum Shading {
		FLAT, SMOOTH, PHONG_TEXTURE, NPR
	};

    
    /**
     * Construct a new game engine.
     *
     * @param camera The camera that is used in the scene.
     */
    public GameEngine(Terrain terrain) {
    	this.world = terrain;
		this.camera = new CameraView(this); //chase cam
    }
    
    /**
     * @see javax.media.opengl.GLEventListener#init(javax.media.opengl.GLAutoDrawable)
     */
    @Override
    public void init(GLAutoDrawable drawable) {
    	//Load all the objects - this allows for texture loading
    	
        // initialise myTime
        GL2 gl = drawable.getGL().getGL2();
        
        myTime = System.currentTimeMillis();
        
        // enable depth testing
        gl.glEnable(GL.GL_DEPTH_TEST);
        // backface culling 
        //gl.glEnable(GL.GL_CULL_FACE);
        //gl.glCullFace(GL.GL_BACK);
        // enable lighting, turn on light 0
        gl.glEnable(GL2.GL_LIGHTING);
        
     	// init shaders
        try {
            initShaders(gl);
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        
        gl.glEnable(GL2.GL_LIGHT0); //sunlight
        gl.glEnable(GL2.GL_LIGHT1); //torch
        // normalise normals (!)
        // this is necessary to make lighting work properly
        gl.glEnable(GL2.GL_NORMALIZE);
        // enable texturing
        gl.glEnable(GL.GL_TEXTURE_2D);
        
        loadModels(gl);

    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
        // ignoreTerrain world
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width,
            int height) {
        // tell the camera and the mouse that the screen has reshaped
        GL2 gl = drawable.getGL().getGL2();
        
        
        //cater for aspect
        camera.reshape(gl, x, y, width, height);
    }


    @Override
    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        
        if(shading == Shading.PHONG_TEXTURE){
            gl.glUseProgram(phong_shaderprogram);
        }else if(shading == Shading.NPR){
             gl.glUseProgram(npr_shaderprogram);
 
    	}else{
    		if(shading == Shading.FLAT){
    			gl.glShadeModel(GL2.GL_FLAT);
    		} else {
    			gl.glShadeModel(GL2.GL_SMOOTH); 
    		}
        	gl.glUseProgram(0);
        }
        
 
        // set the view matrix based on the camera position
        camera.setView(gl); 

        // update the objects
        update();
        // draw the scene tree
        GameObject.ROOT.draw(gl);        
    }

    private void update() {
        // compute the time since the last frame
        long time = System.currentTimeMillis();
        double dt = (time - myTime) / 1000.0;
        myTime = time;
        
        // take a copy of the ALL_OBJECTS list to avoid errors 
        // if new objects are created in the update
        List<GameObject> objects = new ArrayList<GameObject>(GameObject.ALL_OBJECTS);
        
        // update all objects
        for (GameObject g : objects) {
            g.update(dt);
        }        
    }
    

    public void loadModels(GL2 gl) {

    	//Create Lights
    	//Default sun
    	sun = new Light(GameObject.ROOT,5,5,5,true,true,0.1f); //default lighting
    	sun.setPosition(world.getSunlight());
    	
    	
    	
    	float [] blue = {0.0f,0.0f,0.3f,1.0f};
    	float [] orange = {1.0f,0.7f,0.5f,1.0f};
    	float [] white = {0.5f,0.5f,0.5f,1.0f};
    	float [] white2 = {0.5f,0.5f,0.7f,1.0f};
		sun.setAmbient(white);
		sun.setSpecular(white2);
		sun.setDiffuse(white);
    	
    	if(!DEBUG) { 

	    	//Create Models
	    	//Ground
	    	System.out.println("Creating Terrain: "+world.size().height+" x "+world.size().width);
	    	GroundModel ground = new GroundModel(GameObject.ROOT, world, materialList.get("ground_mat"));
	    	
	    	//Create "Trees"
	    	for(Tree tree : world.trees()) {
				TreeModel treeobj = new TreeModel(ground, materialList.get("body_mat"), materialList.get("eyes_mat"), materialList.get("nose_mat"));
				System.out.println("Creating Tree at: "+Arrays.toString(tree.getPosition()));
				treeobj.translate(tree.getPosition());
				treeobj.scale(0.5);
			}
			
			 //Roads
			System.out.println("Creating Road..");
			for(Road road : world.roads()) {
				RoadModel roadobj = new RoadModel(ground, road, materialList.get("road_mat"));
				double[] p = road.point(0);
				//Roads themselves won't go up terrain but they can be built at y
				double y = world.altitude(p[0],p[1]);
				roadobj.translate(0,y,0);
	    	}
    	} else {
    		//Test Object Teaport
        	this.testObj = new TeapotModel(GameObject.ROOT,materialList.get("teapot_mat"));
        	double pos[] = {0.0,4.0,0.0};
        	testObj.translate(pos);
        	
        	//Texture test
        	TextureTestModel test = new TextureTestModel(GameObject.ROOT, world, materialList.get("test_mat"));
        	test.translate(-3, 0.0, -3);
    	}
    }

    private void initShaders(GL2 gl) throws Exception {

    	
    	
    	//Textures
    	Texture bricks = new Texture(gl,"bricks.png","png");
    	Texture snow1 = new Texture(gl,"snow1.jpg","jpg");
    	Texture snow2 = new Texture(gl,"snow2.jpg","jpg");
    	Texture testuv = new Texture(gl,"UV.jpg","jpg");
    	Texture ice2 = new Texture(gl,"ice2.jpg","jpg");
    	//Texture ice3 = new Texture(gl,"ice3.jpg","jpg");
    	Texture road = new Texture(gl,"road2.jpg","jpg");
    	
    	Shader defaultShader = new Shader("defaultShader");
    	Shader eyes = new Shader("defaultShader");
    	Shader nose = new Shader("defaultShader");

    	//Build Materials
    	Material teapot_mat = new Material("teapot_mat", defaultShader, bricks);
    	Material ground_mat = new Material("ground_mat", defaultShader, ice2);
    	Material body_mat = new Material("body_mat", defaultShader, snow2);
    	Material eyes_mat = new Material("eyes_mat", eyes, snow2);
    	Material nose_mat = new Material("nose_mat", nose, snow2);
    	Material road_mat = new Material("road_mat", defaultShader, road);
    	Material test_mat = new Material("test_mat", defaultShader, testuv);
    	
    	materialList.put("teapot_mat", teapot_mat);
    	materialList.put("ground_mat", ground_mat);
    	materialList.put("body_mat", body_mat);
    	materialList.put("eyes_mat", eyes_mat);
    	materialList.put("nose_mat", nose_mat);
    	materialList.put("road_mat", road_mat);
    	materialList.put("test_mat", test_mat);
    	
    	//snowmen
    	float [] black = {0.0f,0.0f,0.0f,1.0f};
    	float [] orange = {1.0f,0.7f,0.5f,1.0f};
    	eyes_mat.getShader().setDiffuse(black);
    	eyes_mat.getShader().setAmbient(black);
    	nose_mat.getShader().setDiffuse(orange);
    	nose_mat.getShader().setAmbient(orange);
    	//build GLSL shaders
    	Material npr_mat = new Material("NPR_mat", defaultShader, ice2);
    	npr_mat.setGLSL(gl,"NPR");
    	
    	Material npr2_mat = new Material("NPR2_mat", defaultShader, ice2);
    	npr2_mat.setGLSL(gl,"NPR2");

    	Material phongTexture_mat = new Material("PhongTexture_mat", defaultShader, ice2);
    	phongTexture_mat.setGLSL(gl,"PhongTexture");
    	
    	//Each shaderProgram must have
        //one vertex shader and one fragment shader.
        npr_shaderprogram = gl.glCreateProgram();
        gl.glAttachShader(npr_shaderprogram, npr_mat.getVertID());
        gl.glAttachShader(npr_shaderprogram, npr_mat.getFragID());
        shaderPrograms.add(npr_shaderprogram);

        npr2_shaderprogram = gl.glCreateProgram();
        gl.glAttachShader(npr2_shaderprogram, npr2_mat.getVertID());
        gl.glAttachShader(npr2_shaderprogram, npr2_mat.getFragID());
        shaderPrograms.add(npr2_shaderprogram);
        
        phong_shaderprogram = gl.glCreateProgram();
        gl.glAttachShader(phong_shaderprogram, phongTexture_mat.getVertID());
        gl.glAttachShader(phong_shaderprogram, phongTexture_mat.getFragID());
        shaderPrograms.add(phong_shaderprogram);
        
        
        for(int program : shaderPrograms) {
        	  gl.glLinkProgram(program);
              int[] error = new int[2];
             gl.glGetProgramiv(program, GL2.GL_LINK_STATUS, error, 0);
              //gl.glGetProgramiv(program, GL2.GL_PROGRAM_SEPARABLE, error, 0);
              if (error[0] != GL2.GL_TRUE) {
              	int[] logLength = new int[1];
                  gl.glGetProgramiv(program, GL2ES2.GL_INFO_LOG_LENGTH, logLength, 0);

                  byte[] log = new byte[logLength[0]];
                  gl.glGetProgramInfoLog(program, logLength[0], (int[]) null, 0, log, 0);

                  System.out.printf("Failed to compile shader! %s\n", new String(log));
                  throw new CompilationException("Error compiling the shader: "
                          + new String(log));
              }
              
              gl.glValidateProgram(program);
              
              gl.glGetProgramiv(program, GL2.GL_VALIDATE_STATUS, error, 0);
              if (error[0] != GL2.GL_TRUE) {
              	System.out.printf("Failed to validate shader!\n");
              	throw new Exception("program failed to validate");
              }
        }
    }
        

 
	public CameraView getCamera() {
		return camera;
	}


	public Terrain getWorld() {
		return world;
	}

	public TeapotModel getTestObj() {
		return testObj;
	}

	public void setTestObj(TeapotModel testObj) {
		this.testObj = testObj;
	}

	public PlayerModel getPlayer() {
		return player;
	}

	public void setPlayer(PlayerModel player) {
		this.player = player;
	}

	public Shading getShading() {
		return shading;
	}

	public void setShading(Shading shading) {
		this.shading = shading;
	}


	
}
