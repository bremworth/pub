package ass2.game;


import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;

import ass2.objects.GameObject;

public class CameraView extends GameObject implements  KeyListener, MouseMotionListener{ 
	private GameEngine engine;
	private Point mousePoint;
	private double angle = 2.3;
    private static final int ROTATION_SCALE = 1;
    private static final float MOVE_FRACTION = 0.1f;
    private static final float ROTATE_FRACTION = 0.1f;
    private int rotateCameraX = 0;
    private int rotateCameraY = 0;
    private int translateCameraX = 0;
    private int translateCameraY = 0;
    private int translateCameraZ = 5;
    private double lx = 3.0;
    private double ly = 0.0;
    private double lz = 3.0;
    private double camx = -2.0;
    private double camy = 0.0;
    private double camz = -2.0;
    private boolean DEBUG = false;
    

    public CameraView(GameObject parent,GameEngine engine) {
    	super(parent, "Camera");
    	this.engine = engine;
	}
  

    public CameraView(GameEngine engine) {
    	super(GameObject.ROOT, "Camera");
    	this.engine = engine;
	}


    public void setView(GL2 gl) {

    	gl.glClearColor(1, 1, 1, 1);
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
        //System.out.println("CamX: "+camx+" CamY: "+camy+" CamZ: "+camz+" lx: "+lx+" lz: "+lz+" angle: "+angle);
        	
        GLU glu = new GLU();

        double gety = engine.getWorld().altitude(camx, camz-1.0); 
        if(gety != -1)  //out of terrain
        	camy = gety;
        
        if(DEBUG) {
        	glu.gluLookAt(2.0, 2.0, 2.0, 5.0, 0.0,5.0, 0.0, 1.0, 0.0);
        } else {
        	glu.gluLookAt(camx,camy+1.0f, camz, camx+lx, camy+1.0f, camz+lz, 0.0, 1.0, 0.0); 
        }
        //mouse look
        gl.glRotated(rotateCameraX, 1, 0, 0);
        gl.glRotated(rotateCameraY, 0, 1, 0);
  
    }
    
    public void reshape(GL2 gl, int x, int y, int width, int height) {
    	double aspectRatio = 1.0 * width / height;
    	
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();

        GLU glu = new GLU();
        glu.gluPerspective(60,aspectRatio,0.1f,50); 

    }


	@Override
	public void mouseDragged(MouseEvent e) {
	       Point p = e.getPoint();
	        if (mousePoint != null) {
	            int dx = p.x - mousePoint.x;
	            int dy = p.y - mousePoint.y;

	            if (e.getButton() == MouseEvent.MOUSE_FIRST || true) {
	            	rotateCameraY += dx * ROTATION_SCALE;
	            	rotateCameraX += dy * ROTATION_SCALE;
	            }
	        }
	        mousePoint = p;
	}


	@Override
	public void mouseMoved(MouseEvent e) {
		 mousePoint = e.getPoint();
		
	}


	@Override
	public void keyPressed(KeyEvent e) {
	    
		   switch (e.getKeyCode()) {
		   case KeyEvent.VK_UP: 
			   camx += lx * MOVE_FRACTION;
			   camz += lz * MOVE_FRACTION;
			   break;
		   case KeyEvent.VK_DOWN: 
			   camx -= lx * MOVE_FRACTION;
			   camz -= lz * MOVE_FRACTION;
			   break;
		   case KeyEvent.VK_LEFT: 
			   	angle -= ROTATE_FRACTION;
			   	lx = Math.sin(angle);
			   	lz = -Math.cos(angle);
			   break;
		   case KeyEvent.VK_RIGHT: //
			   angle += ROTATE_FRACTION;
			   lx = Math.sin(angle);
			   lz = -Math.cos(angle);
			   break;
		   case KeyEvent.VK_E: 
			      camx += lx * MOVE_FRACTION;
			      camz += lz * MOVE_FRACTION;
			   break;
		   case KeyEvent.VK_D: 
			     camx -= lx * MOVE_FRACTION;
			     camz -= lz * MOVE_FRACTION;
				  break;  
		   case KeyEvent.VK_S:
			   	angle -= ROTATE_FRACTION; 
			   	lx = Math.sin(angle);
			   	lz = -Math.cos(angle);
				  break;
		   case KeyEvent.VK_F:
			   angle += ROTATE_FRACTION;
			   lx = Math.sin(angle);
			   lz = -Math.cos(angle);
				  break;
		   case KeyEvent.VK_A: //test smooth shading
			    engine.getTestObj().setSmooth(!engine.getTestObj().isSmooth()); //hrmm
		        break;
		   case KeyEvent.VK_SPACE: //switch shading modes
				  engine.setShading(engine.getShading().values()[(engine.getShading().ordinal() + 1)%4]); 
			      break;

		 default:
			 break;
		 }
		
	}


	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
