package ass2.game;


/**
 * A collection of useful math methods 

 * @author malcolmr
 */
public class MathUtil {

    /**
     * Normalise an angle to the range (-180, 180]
     * 
     * @param angle 
     * @return
     */
    public static double normaliseAngle(double angle) {
        return ((angle + 180.0) % 360.0 + 360.0) % 360.0 - 180.0;
    }
    
    public static double lerp(double[] p0, double[] p1) {
    	
    	return 1.0;
    }
    
    public static double normalize(double v[]) {
        double length;
        
        length = Math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
        v[0] /= length;
        v[1] /= length;
        v[2] /= length;
        
        return length;
    }
    
    public static double getMagnitude(double [] n){
    	double mag = n[0]*n[0] + n[1]*n[1] + n[2]*n[2];
    	mag = Math.sqrt(mag);
    	return mag;
    }
    
    public static double [] normalise(double [] n){
    	double  mag = getMagnitude(n);
    	double norm[] = {n[0]/mag,n[1]/mag,n[2]/mag};
    	return norm;
    }
    
    public static double [] cross(double u [], double v[]){
    	double crossProduct[] = new double[3];
    	crossProduct[0] = u[1]*v[2] - u[2]*v[1];
    	crossProduct[1] = u[2]*v[0] - u[0]*v[2];
    	crossProduct[2] = u[0]*v[1] - u[1]*v[0];
    	//System.out.println("CP " + crossProduct[0] + " " +  crossProduct[1] + " " +  crossProduct[2]);
    	return crossProduct;
    }
    
    public static double [] getNormal(double[] p0, double[] p1, double[] p2){
    	double u[] = {p1[0] - p0[0], p1[1] - p0[1], p1[2] - p0[2]};
    	double v[] = {p2[0] - p0[0], p2[1] - p0[1], p2[2] - p0[2]};
    	
    	return normalise(cross(u,v));
    	
    }

    /**
     * Clamp a value to the given range
     * 
     * @param value
     * @param min
     * @param max
     * @return
     */

    public static double clamp(double value, double min, double max) {
        return Math.max(min, Math.min(max, value));
    }
    
    /**
     * Multiply two matrices
     * 
     * @param p A 3x3 matrix
     * @param q A 3x3 matrix
     * @return
     */
    public static double[][] multiply(double[][] p, double[][] q) {

        double[][] m = new double[3][3];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                m[i][j] = 0;
                for (int k = 0; k < 3; k++) {
                   m[i][j] += p[i][k] * q[k][j]; 
                }
            }
        }

        return m;
    }

    /**
     * Multiply a vector by a matrix
     * 
     * @param m A 3x3 matrix
     * @param v A 3x1 vector
     * @return
     */
    public static double[] multiply(double[][] m, double[] v) {

        double[] u = new double[3];

        for (int i = 0; i < 3; i++) {
            u[i] = 0;
            for (int j = 0; j < 3; j++) {
                u[i] += m[i][j] * v[j];
            }
        }

        return u;
    }

    public static double[][] multiply3d(double[][] p, double[][] q) {

        double[][] m = new double[4][4];

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                m[i][j] = 0;
                for (int k = 0; k < 4; k++) {
                   m[i][j] += p[i][k] * q[k][j]; 
                }
            }
        }

        return m;
    }

    public static double[] multiply3d(double[][] m, double[] v) {

        double[] u = new double[4];

        for (int i = 0; i < 4; i++) {
            u[i] = 0;
            for (int j = 0; j < 4; j++) {
                u[i] += m[i][j] * v[j];
            }
        }

        return u;
    }


        
    
    public static double[][] translationMatrix(double[] v) {
    	double[][] t = identity2d();
    	t[0][2] = v[0];
        t[1][2] = v[1];
        return t;
    }
    public static double[][] translationMatrix3d(double[] v) {
    	double[][] t = identity3d();
    	t[0][3] = v[0];
        t[1][3] = v[1];
        t[2][3] = v[2];
        return t;
    }
    public static double[][] rotationMatrix(double angle) {
    	double rads = Math.toRadians(angle);
        double[][] r = identity2d();
	     r[0][0] = Math.cos(rads); r[0][1] = -Math.sin(rads);
	     r[1][0] = Math.sin(rads); r[1][1] = Math.cos(rads);

        return r;
    }

    public static double[][] scaleMatrix(double scale) {
    	double[][] s = identity2d();
        s[0][0] = scale; //Sx
        s[1][1] = scale; //Sy
        return s;
    }
    
    public static double[][] identity2d() {
    	double[][] r = {{1,0,0},
						{0,1,0},
						{0,0,1}};
    	return r;
    }
    public static double[][] identity3d() {
    	double[][] r = {{1,0,0,0},
						{0,1,0,0},
						{0,0,1,0},
    					{0,0,0,1}};
    	return r;
    }

    public static void print(double[][] m) {

       

        for (int i = 0; i < 4; i++) {
            
            for (int j = 0; j < 4; j++) {
                System.out.print(m[i][j] + " " );
            }
            System.out.println();
        }
    }
}
