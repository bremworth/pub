#version 120

varying vec3 normal;
varying vec3 v;
varying vec3 lightDir;

void main (void) {	
	
    v = vec3(gl_ModelViewMatrix * gl_Vertex);
    normal = normalize(gl_NormalMatrix * gl_Normal);
    lightDir = normalize(vec3(gl_LightSource[0].position.xyz - v));

	gl_Position = ftransform();
	
}

