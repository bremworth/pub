#version 120

varying vec3 normal;
varying vec3 v;
varying vec3 lightDir;

void main (void) {	
	float intensity;
	vec4 color;
	
	intensity = dot(lightDir,normalize(normal));

	if (intensity > 0.95)
		color = vec4(0.8,0.8,0.9,1.0);
	else if (intensity > 0.4)
		color = vec4(0.6,0.6,0.7,1.0);
	else if (intensity > 0.1)
		color = vec4(0.4,0.4,0.5,1.0);
	else
		color = vec4(0.3,0.3,0.5,1.0);
	gl_FragColor = color;

	
}

