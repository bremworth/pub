#version 120


varying vec3 N;
varying vec3 v;

/* We are only taking into consideration light0 and assuming it is a point light */
void main (void) {	
    /* frontLightModelProduct.sceneColor = emissive + ambCoeff*globalAmbient */
    
    vec4 ambient = gl_FrontLightModelProduct.sceneColor + gl_FrontLightProduct[0].ambient;
	ambient = clamp(ambient,0,1);
	
	/* Diffuse calculations */

	vec3 normal, lightDir; 
	
	vec4 diffuse;
	float NdotL;
	
	/* normal has been interpolated and may no longer be unit length so we need to normalise*/
	normal = normalize(N);
	
	
	/* normalize the light's direction. */
	lightDir = normalize(vec3(gl_LightSource[0].position.xyz - v));
    NdotL = max(dot(normal, lightDir), 0.0); 
    /* Compute the diffuse term */
     diffuse = NdotL * gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse; 

    vec4 specular = vec4(0.0,0.0,0.0,1);
    float NdotHV;
    float NdotR;
    vec3 dirToView = normalize(-v);
    
    vec3 R = normalize(-reflect(lightDir,normal)); 
   
    /* compute the specular term if NdotL is  larger than zero */
    
	if (NdotL > 0.0) {
		NdotR = max(dot(R,dirToView ),0.0);
		/*
		Can use the halfVector instead of the reflection vector if you wish 
		NdotHV = max(dot(normal, normalize(gl_LightSource[0].halfVector.xyz)),0.0);
		*/
		specular = gl_FrontMaterial.specular * gl_LightSource[0].specular * pow(NdotR,gl_FrontMaterial.shininess);
	}
	
	
	
 	
	gl_FragColor = ambient + diffuse + specular; 

	
}

