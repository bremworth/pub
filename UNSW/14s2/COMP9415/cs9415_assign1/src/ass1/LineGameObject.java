package ass1;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

public class LineGameObject extends GameObject {
	private double[] p1 = new double[2];
    private double[] p2 = new double[2];
    private double[] myLineColour = {0,0,0,1};;
    
	//Create a LineGameObject from (0,0) to (1,0)
	public LineGameObject(GameObject parent, double[] lineColour){
		super(parent);
		p1[0] = 0;	p1[1] = 0;
		p2[0] = 1;	p2[1] = 0; 
        myLineColour = lineColour;
	}

	//Create a LineGameObject from (x1,y1) to (x2,y2)
	public LineGameObject(GameObject parent,  double x1, double y1,double x2, double y2,
	                                          double[] lineColour){
		super(parent);
		p1[0] = x1;	p1[1] = y1;
		p2[0] = x2;	p2[1] = y2;
        myLineColour = lineColour;
	}
	

    /**
     * Get the line
     * 
     * @return
     */
    public double[][] getPoints() {
    	double[][] points = {{0,0},{0,0}};
    	points[0][0] = p1[0]; points[0][1] = p1[1];
    	points[1][0] = p2[0]; points[1][1] = p2[1];
        return points;
    }

    /**
     * Set the line
     * 
     * @param points
     */
    public void setPoints(double[] p1, double[] p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    /**
     * Get the line colour.
     * 
     * @return
     */
    public double[] getLineColour() {
        return myLineColour;
    }

    /**
     * Set the line colour.
     * 
     * Setting the colour to null means the line should not be drawn
     * 
     * @param lineColour
     */
    public void setLineColour(double[] lineColour) {
        myLineColour = lineColour;
    }

    // ===========================================
    // COMPLETE THE METHODS BELOW
    // ===========================================
    

    /**
     * TODO: Draw the polygon
     * 
     * if the line colour is non-null, draw the line with this colour
     * 
     * @see ass1.spec.GameObject#drawSelf(javax.media.opengl.GL2)
     */
    @Override
    public void drawSelf(GL2 gl) {
    	
    	if(myLineColour == null) 
    		return;
    	
    	gl.glLineWidth(1);
    	gl.glColor4d(myLineColour[0], myLineColour[1], myLineColour[2], myLineColour[3]);
    	gl.glBegin(GL.GL_LINES);
        {
            gl.glVertex2d(p1[0], p1[1]);
            gl.glVertex2d(p2[0], p2[1]);
        }
        gl.glEnd();

    }


}
