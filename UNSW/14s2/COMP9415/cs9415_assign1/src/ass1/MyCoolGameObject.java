package ass1;



public class MyCoolGameObject extends GameObject {
	private GameObject myParent;
	
	public MyCoolGameObject(GameObject parent) {
		super(parent);
		myParent = parent;
		createFlower();
	}

	public MyCoolGameObject() {
		super(GameObject.ROOT);
		myParent = GameObject.ROOT;
		createFlower();
	}

	public void createFlower() {

        double yellow[] = {1,1,0,1};
        double yellow2[] = {1,0.8,0,1};
        double brown[] = {0.5,0.2,0.2,1};
        double green[] = {0,1,0,1};
        double darkgreen[] = {0,0.2,0,1};
        double blackish[] = {0.3,0.1,0.1,1};
        double black[] = {0.0,0.0,0.0,1};


        //Create a flower object
        //Stem
        double points[] = {0.05,0,0.05,-10,-0.05,-10,-0.05,0};
        PolygonalGameObject stem = new PolygonalGameObject(this,points,green,darkgreen);
        StarGameObject outter = new StarGameObject(stem,1,0.7,24,yellow);
        StarGameObject outter2 = new StarGameObject(outter,0.7,0.5,20,yellow2);
        StarGameObject inner = new StarGameObject(outter2,0.4,0.3,32,brown);
        CircularGameObject centre = new CircularGameObject(inner,blackish,black);
        centre.setScale(0.25);

	}
}
