package ass1;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

/**
 * A game object that has a polygonal shape.
 * 
 * This class extend GameObject to draw polygonal shapes.
 *
 * TODO: The methods you need to complete are at the bottom of the class
 *
 * @author malcolmr
 */
public class StarGameObject extends GameObject {

    private int myPoints;
    private double innerRadius;
    private double outterRadius;
    private double[] myFillColour = {0,0,0,1};

    /**

     */
    public StarGameObject(GameObject parent, double radius1,double radius2, int num,
            double[] fillColour) {
        super(parent);
        innerRadius = radius1;
        outterRadius = radius2;
        myPoints = num;
        myFillColour = fillColour;
    }

    

    public int getMyPoints() {
		return myPoints;
	}



	public void setMyPoints(int myPoints) {
		this.myPoints = myPoints;
	}



	public double getInnerRadius() {
		return innerRadius;
	}



	public void setInnerRadius(double innerRadius) {
		this.innerRadius = innerRadius;
	}



	public double getOutterRadius() {
		return outterRadius;
	}



	public void setOutterRadius(double outterRadius) {
		this.outterRadius = outterRadius;
	}



	/**
     * Get the fill colour
     * 
     * @return
     */
    public double[] getFillColour() {
        return myFillColour;
    }

    /**
     * Set the fill colour.
     * 
     * Setting the colour to null means the object should not be filled.
     * 
     * @param fillColour The fill colour in [r, g, b, a] form 
     */
    public void setFillColour(double[] fillColour) {
        myFillColour = fillColour;
    }

    @Override
    public void drawSelf(GL2 gl) {
    	
    	if(myFillColour != null) {
        	double x = 0.0;
        	double y = 0.0;
    		gl.glColor3d(myFillColour[0], myFillColour[1], myFillColour[2]);
    		gl.glBegin(GL2.GL_TRIANGLE_FAN);
    		{
	    		gl.glVertex2d(x, y); // center of circle
	    		for(int i = 0; i <= myPoints; ++i) {
	    			gl.glVertex2d(x + (outterRadius * Math.cos(i * Math.PI * 2 / myPoints)),
	    						  y + (outterRadius * Math.sin(i * Math.PI * 2 / myPoints)));
	    			gl.glVertex2d(x + (innerRadius * Math.cos(i * Math.PI * 2  / myPoints)),
  						          y + (innerRadius * Math.sin(i * Math.PI * 2  / myPoints)));
	    		}
    		}
    		gl.glEnd();
    	}
    }


}
