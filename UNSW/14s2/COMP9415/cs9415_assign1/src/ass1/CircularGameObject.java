package ass1;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

public class CircularGameObject extends GameObject {
	private double myRadius;
    private double[] myFillColour = {0,0,0,1};;
    private double[] myLineColour = {0,0,0,1};;
    
	//Create a CircularGameObject with centre 0,0 and radius 1
	public CircularGameObject(GameObject parent, double[] fillColour,
	                                             double[] lineColour){
		super(parent);
		myRadius = 1.0;
        myFillColour = fillColour;
        myLineColour = lineColour;
	}

	//Create a CircularGameObject with centre 0,0 and a given radius
	public CircularGameObject(GameObject parent, double radius,double[] fillColour,
	                                                           double[] lineColour){
		super(parent);
		myRadius = radius;
        myFillColour = fillColour;
        myLineColour = lineColour;
	}


    /**
     * Get the polygon
     * 
     * @return
     */
    public double getRadius() {        
        return myRadius;
    }

    /**
     * Set the polygon
     * 
     * @param points
     */
    public void setRadius(double radius) {
    	myRadius = radius;
    }

    /**
     * Get the fill colour
     * 
     * @return
     */
    public double[] getFillColour() {
        return myFillColour;
    }

    /**
     * Set the fill colour.
     * 
     * Setting the colour to null means the object should not be filled.
     * 
     * @param fillColour The fill colour in [r, g, b, a] form 
     */
    public void setFillColour(double[] fillColour) {
        myFillColour = fillColour;
    }

    /**
     * Get the outline colour.
     * 
     * @return
     */
    public double[] getLineColour() {
        return myLineColour;
    }

    /**
     * Set the outline colour.
     * 
     * Setting the colour to null means the outline should not be drawn
     * 
     * @param lineColour
     */
    public void setLineColour(double[] lineColour) {
        myLineColour = lineColour;
    }

    // ===========================================
    // COMPLETE THE METHODS BELOW
    // ===========================================
    

    /**
     * TODO: Draw the polygon
     * 
     * if the fill colour is non-null, fill the polygon with this colour
     * if the line colour is non-null, draw the outline with this colour
     * 
     * @see ass1.spec.GameObject#drawSelf(javax.media.opengl.GL2)
     */
    @Override
    public void drawSelf(GL2 gl) {

    	
         if(this.getFillColour() != null) {
     		
 	    	 gl.glBegin(GL2.GL_POLYGON);
 	         {
 	        	gl.glColor4d(myFillColour[0],myFillColour[1],myFillColour[2],myFillColour[3]);
 	        	for (int i = 0; i < 32; i++) { //approximate circle as 32 sided polygons
 	               double a = i * Math.PI * 2 / 32;

 	               double x = myRadius*Math.cos(a);
 	               double y = myRadius*Math.sin(a); // off centre

 	               gl.glVertex2d(x, y);
 	           }
 	         }
 	         gl.glEnd();
     	}
         if(this.getLineColour() != null){

     	 //Draw the outline
     	 gl.glBegin(GL2.GL_LINE_LOOP);
     	 
     	gl.glLineWidth(1);
     	gl.glColor4d(myLineColour[0],myLineColour[1],myLineColour[2],myLineColour[3]);
         for (int i = 0; i < 32; i++) { //approximate circle as 32 sided polygons
              double a = i * Math.PI * 2 / 32;

              double x = myRadius*Math.cos(a);
              double y = myRadius*Math.sin(a); 

              gl.glVertex2d(x, y);
          }

          gl.glEnd();
         }
    }


}
