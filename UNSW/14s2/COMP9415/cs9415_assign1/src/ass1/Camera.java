package ass1;

import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;


/**
 * The camera is a GameObject that can be moved, rotated and scaled like any other.
 * 
 * TODO: You need to implment the setView() and reshape() methods.
 *       The methods you need to complete are at the bottom of the class
 *
 * @author malcolmr
 */
public class Camera extends GameObject {

    private float[] myBackground;

    public Camera(GameObject parent) {
        super(parent);

        myBackground = new float[4];
    }

    public Camera() {
        this(GameObject.ROOT);
    }
    
    public float[] getBackground() {
        return myBackground;
    }

    public void setBackground(float[] background) {
        myBackground = background;
    }

    // ===========================================
    // COMPLETE THE METHODS BELOW
    // ===========================================
   
    
    public void setView(GL2 gl) {
    	
        //  1. clear the view to the background colour
    	gl.glClearColor(myBackground[0],myBackground[1],myBackground[2],myBackground[3]);
    	gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
    	
    	
        // 2. set the view matrix to account for the camera's position  ?!?!?   
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
        
        gl.glScaled(1/getGlobalScale(), 1/getGlobalScale(), 1);
        gl.glRotated(-getGlobalRotation(), 0, 0, 1);
        gl.glTranslated(-getGlobalPosition()[0], -getGlobalPosition()[1], 0);
   

    }

    public void reshape(GL2 gl, int x, int y, int width, int height) {
        //   1. match the projection aspect ratio to the viewport
        // to avoid stretching 

    	double aspect = 1.0 * width / height;
    	gl.glViewport(x, y, width, height);
    	
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        
        double left = -1.0;
        double right = 1.0;
        double top = -1.0;
        double bottom = 1.0;
        
        GLU glu = new GLU();
        if(aspect < 1.0){
            glu.gluOrtho2D(left, right, top/aspect, bottom/aspect);
        } else {
        	glu.gluOrtho2D(left*aspect, right*aspect, top, bottom);
        }
        
    }
}
