package ass1;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

/**
 * A game object that has a polygonal shape.
 * 
 * This class extend GameObject to draw polygonal shapes.
 *
 * TODO: The methods you need to complete are at the bottom of the class
 *
 * @author malcolmr
 */
public class TriangleGameObject extends GameObject {

    private int myTriangles;
    private double myRadius;
    private double[] myFillColour = {0,0,0,1};

    /**
     * Create a polygonal game object and add it to the scene tree
     * 
     * The polygon is specified as a list of doubles in the form:
     * 
     * [ x0, y0, x1, y1, x2, y2, ... ]
     * 
     * The line and fill colours can possibly be null, in which case that part of the object
     * should not be drawn.
     *
     * @param parent The parent in the scene tree
     * @param points A list of points defining the polygon
     * @param fillColour The fill colour in [r, g, b, a] form
     * @param lineColour The outline colour in [r, g, b, a] form
     */
    public TriangleGameObject(GameObject parent, double radius, int triangles,
            double[] fillColour) {
        super(parent);
        myRadius = radius;
        myTriangles = triangles;
        myFillColour = fillColour;
    }

    
    public double getMyRadius() {
		return myRadius;
	}


	public void setMyRadius(double myRadius) {
		this.myRadius = myRadius;
	}


	/**
     * Get the polygon
     * 
     * @return
     */
    public int getPoints() {        
        return myTriangles;
    }

    /**
     * Set the polygon
     * 
     * @param points
     */
    public void setPoints(int triangles) {
    	myTriangles = triangles;
    }

    /**
     * Get the fill colour
     * 
     * @return
     */
    public double[] getFillColour() {
        return myFillColour;
    }

    /**
     * Set the fill colour.
     * 
     * Setting the colour to null means the object should not be filled.
     * 
     * @param fillColour The fill colour in [r, g, b, a] form 
     */
    public void setFillColour(double[] fillColour) {
        myFillColour = fillColour;
    }

    @Override
    public void drawSelf(GL2 gl) {
    	
    	if(myFillColour != null) {
        	double x = 0.0;
        	double y = 0.0;
    		gl.glColor3d(myFillColour[0], myFillColour[1], myFillColour[2]);
    		gl.glBegin(GL2.GL_TRIANGLE_FAN);
    		{
	    		gl.glVertex2d(x, y); // center of circle
	    		for(int i = 0; i <= myTriangles; ++i) {
	    			//Outter points
	    			gl.glVertex2d(x + (myRadius * Math.cos(i * Math.PI * 2 / myTriangles)),
	    						  y + (myRadius * Math.sin(i * Math.PI * 2  / myTriangles)));
	    			//Inner points
	    			gl.glVertex2d(x + ((myRadius/1.5) * Math.cos(i * Math.PI * 2 / myTriangles)),
  						          y + ((myRadius/1.5) * Math.sin(i * Math.PI * 2 / myTriangles)));
	    		}
    		}
    		gl.glEnd();
    	}
    }


}
