
#include "btree.h"
#include "btree_iterator.h"
#include <iostream>

#include <map>
#include <set>

//std::map m;
//std::set s;
//std::_Rb_tree t;

using namespace std;

#include <map>
#include <set>

//std::map m;
//std::set s;
//std::_Rb_tree t;

int test2();
int test3();
int test4();

int main(void) {

    btree<int> t(3);

    t.insert(1000545);
    t.insert(3450521);
    t.insert(1543561);
    t.insert(1000591);
    t.insert(540055);
    t.insert(4320057);
    t.insert(7634058);
    t.insert(10005199);
    t.insert(4230531);
    t.insert(1000523);
    t.insert(234051);
    t.insert(1432059);
    t.insert(1000517);
    t.insert(100054);
    t.insert(1000523);
    t.insert(234052);
    t.insert(100056);
    t.bfs();
    auto findtest1 = t.find(1000545);
    if(findtest1 == t.end())
      cout << "Not Found 1 " << endl;
     findtest1 = t.find(3450521);
    if(findtest1 == t.end())
      cout << "Not Found 2" << endl;
     findtest1 = t.find(1543561);
    if(findtest1 == t.end())
      cout << "Not Found 3" << endl;
     findtest1 = t.find(1000591);
    if(findtest1 == t.end())
      cout << "Not Found 4" << endl;
     findtest1 = t.find(540055);
    if(findtest1 == t.end())
      cout << "Not Found 5" << endl;
     findtest1 = t.find(4320057);
    if(findtest1 == t.end())
      cout << "Not Found 6" << endl;
     findtest1 = t.find(7634058);
    if(findtest1 == t.end())
      cout << "Not Found 7" << endl;
     findtest1 = t.find(10005199);
    if(findtest1 == t.end())
      cout << "Not Found 8" << endl;
     findtest1 = t.find(4230531);
    if(findtest1 == t.end())
      cout << "Not Found 9" << endl;
     findtest1 = t.find(1000523);
    if(findtest1 == t.end())
      cout << "Not Found 10" << endl;
     findtest1 = t.find(234051);
    if(findtest1 == t.end())
      cout << "Not Found 11" << endl;
     findtest1 = t.find(1432059);
    if(findtest1 == t.end())
      cout << "Not Found 12" << endl;
      findtest1 = t.find(1000517);
    if(findtest1 == t.end())
      cout << "Not Found 13" << endl;
     findtest1 = t.find(100054);
    if(findtest1 == t.end())
      cout << "Not Found 14" << endl;
     findtest1 = t.find(1000523);
    if(findtest1 == t.end())
      cout << "Not Found 15" << endl;
     findtest1 = t.find(234052);
    if(findtest1 == t.end())
      cout << "Not Found 16" << endl;
    findtest1 = t.find(100056);
    if(findtest1 == t.end())
      cout << "Not Found 17" << endl;

    for(auto it = t.begin(); it != t.end(); ++it)
         cout << *it << " ";
    cout << endl;
    for(auto it = t.begin(); it != t.end(); ++it)
         cout << *it << " ";
    cout << endl;
    for(auto it = t.cbegin(); it != t.cend(); ++it)
         cout << *it << " ";
    cout << endl;
    for(auto it = t.cbegin(); it != t.cend(); ++it)
         cout << *it << " ";
    cout << endl;

    cout << t << endl;

    auto it = t.cbegin();
    cout << *it << " " << endl;
    ++it;
    cout << *it << " " << endl;
    it++;
    cout << *it << " " << endl;
//    //copy assignment
    //btree<int> tree4 = &t;
    //move
    //btree<int> tree5 = std::move(&t);

//   test2();
//    test3();
//    test4();

    return 1;
}

#include <algorithm>
#include <cstddef>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <set>
#include <string>

#include "btree.h"
using std::endl;
using std::ifstream;
using std::ofstream;
using std::set;
using std::string;

namespace {

const long kMinInteger = 1000000;
const long kMaxInteger = 100000000;

void initRandom(unsigned long);
long getRandom(long low, long high);
void insertRandomNumbers(btree<long>&, set<long>&, size_t);
bool confirmEverythingMatches(const btree<long>&, const set<long>&);

/**
 * Initialises random number generator.
 * The default argument uses a 'random' seed, but a specific
 * seed can be supplied to reproduce the random sequence for testing.
 **/
void initRandom(unsigned long seed = 0) {
  if (seed == 0) {
    srandom(time(NULL));
  } else {
    srandom(seed);
  }
}

/**
 * Produces a random number in the given range.
 * The random() function provides better quality random numbers
 * than the older rand() function.
 **/
long getRandom(long low, long high) {
  return (low + (random() % ((high - low) + 1)));
}

/**
 * Tries to insert numbers into the specified test container, and every time
 * the insertion succeeds (because the number wasn't previously in the btree),
 * the same number is inserted into the off-the-shelf set
 * class instance so we can later track the correctness of our btree.
 **/
void insertRandomNumbers(btree<long>& testContainer, set<long>& stableContainer, size_t size) {
  cout << "Let's insert up to " << size << " numbers." << endl;
  for (size_t i = 0; i < size; i++) {
    long rndNum = getRandom(kMinInteger, kMaxInteger);
    pair<btree<long>::iterator, bool> result = testContainer.insert(rndNum);
    if (result.second) stableContainer.insert(rndNum);
    if ((i + 1) % 100000 == 0)
      cout << "Inserted some " << (i + 1) << " numbers thus far." << endl;
  }
  cout << endl;
}

/**
 * Confirms that the specified btree and the specified
 * set contain exactly the same numbers.  This does so by
 * considering all numbers that could have been added, and
 * asserting that each of those numbers is either present
 * in or absent from both containers.  If all checks out,
 * we return 0 to signal success; if along the way we see
 * an integer in one container and not the other, then
 * we bail immediately and return one to express failure.
 **/
bool confirmEverythingMatches(const btree<long>& testContainer, const set<long>& stableContainer) {
  cout << "Confirms the btree and the set "
          "contain exactly the same values..." << endl;
  for (long i = kMinInteger; i <= kMaxInteger; i++) {

    bool foundInTree = (testContainer.find(i) != testContainer.end());
    bool foundInSet = (stableContainer.find(i) != stableContainer.end());
    if (foundInTree != foundInSet) {
      cout << i << endl;
      cout << "- btree and set don't contain the same data!" << endl;
      cout << "Mismatch at element: " << i << endl;
      //testContainer.bfs();
      for(auto it = testContainer.begin(); it != testContainer.end(); ++it)
          cout << *it << " ";
      return false;
    }
  }
  cout << "- btree checks out just fine." << endl;

  return true;
}

}  // namespace close


/**
 * Codes for testing various bits and pieces. Most of the code is commented out
 * you should uncomment it as appropriate.
 **/
int test2(void) {
  // initialise random number generator with 'random' seed
  initRandom();

  // insert lots of random numbers and compare with a known correct container
  btree<long> testContainer(99);//99
  set<long> stableContainer;

//  insertRandomNumbers(testContainer, stableContainer, 1000000); //1000000
//  btree<long> btcpy = testContainer;
//  confirmEverythingMatches(btcpy, stableContainer);



  // this next portion was something I used to sort a bunch of chars
  // this was what I used to debug my iterator and made it work
    btree<char> astring;

    cout << "\nInserting these random chars into the tree...\n";
    for(int i = 0; i < 10; i++) {
        pair<btree<char>::iterator, bool> result = astring.insert(static_cast<char>(getRandom('A', 'z')));
        cout << *result.first;
    }
    cout << endl << endl;

    for(btree<char>::iterator iter = astring.begin(); iter != astring.end(); ++iter)
    cout << *iter;
    cout << "Sorting some chars end" << endl;

  // const iterator test
  const btree<char>& refstring = astring;
    btree<char>::const_iterator iter;
    cout << "Voila!  Sorted!" << endl;
    for(iter = refstring.begin(); iter != refstring.end(); ++iter) {
        astring.insert(static_cast<char>(getRandom('A', 'z')));

        cout << *(iter) << " ";
    }

    for(btree<char>::const_iterator iter = refstring.begin(); !(iter == refstring.end()); ++iter)
        cout << *iter;
    cout << endl;

  cout << "a full-scale string test of the tree using iterators" << endl;
  btree<string> *strTable = new btree<string>(40);

  ifstream wordFile("twl.txt");
  if (!wordFile)
    return 1;  // file couldn't be opened for some reason, abort...

  while (wordFile.good()) {
    string word;
    getline(wordFile, word);
    strTable->insert(word);
  }
  wordFile.close();

  cout << "twl.txt sorted by our wonderful tree..." << endl;
  // Such beautiful code with iterators...
  for(btree<string>::const_iterator iter = strTable->begin(); iter != strTable->end(); ++iter)
       cout << *iter << endl;
//   for(auto iter = strTable->begin(); iter != strTable->end(); ++iter)
//      cout << *iter << endl;

  // reverse iterator
  btree<string>::reverse_iterator riter = strTable->rbegin();
  btree<string>::const_iterator citer = strTable->begin();

//  auto riter = strTable->rbegin();
 //auto citer = strTable->begin();

  if (*citer != *riter) {
    cout << "success!" << endl;
  }

  // try to create a copy
  btree<string> btcpy2;

  btcpy2 = *strTable;

  ofstream ofs1("out1");
  ofstream ofs2("out2");

  copy(strTable->begin(), strTable->end(), ostream_iterator<string>(ofs1, " "));
  ofs1 << endl;
  ofs1 << *strTable << endl;

  delete strTable;

  copy(btcpy2.begin(), btcpy2.end(), ostream_iterator<string>(ofs2, " "));
  ofs2 << endl;
  ofs2 << btcpy2 << endl;

  ofs1.close();
  ofs2.close();



  return 0;
}
#include <algorithm>
#include <iostream>
#include <iterator>

#include "btree.h"

void foo(const btree<int> &b) {
  std::copy(b.begin(), b.end(), std::ostream_iterator<int>(std::cout, " "));
  std::cout << std::endl;
}

int test3(void) {
  btree<int> b;

  b.insert(1);
  b.insert(10);
  b.insert(3);
  b.insert(4);

  for(btree<int>::iterator iter = b.begin(); iter != b.end(); ++iter)
    std::cout << *iter << std::endl;

  foo(b);

  return 0;
}
#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>

#include "btree.h"

template <typename T>
void find_in_tree(const btree<T> &b, T val) {
  auto iter = std::find(b.begin(), b.end(), val);
  if (iter == b.end())
    std::cout << val << " not found" << std::endl;
  else
    std::cout << val << " found" << std::endl;
}

int test4(void) {
  btree<std::string> bts;

  bts.insert("comp3000");
  bts.insert("comp6771");
  bts.insert("comp2000");
  bts.insert("comp1000");

  find_in_tree(bts, std::string("comp6771"));

  btree<int> bti;

  bti.insert(1);
  bti.insert(10);
  bti.insert(3);
  bti.insert(4);

  find_in_tree(bti, 100);

  return 0;
}
