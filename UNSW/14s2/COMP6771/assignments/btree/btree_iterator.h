#ifndef BTREE_ITERATOR_H
#define BTREE_ITERATOR_H

#include <iterator>
#include <list>

/**
 * You MUST implement the btree iterators as (an) external class(es) in this file.
 * Failure to do so will result in a total mark of 0 for this deliverable.
 **/

template <typename T> class btree; //for the friend

// iterator related interface stuff here; would be nice if you called your
// iterator class btree_iterator (and possibly const_btree_iterator)
template <typename T>
struct btree_iterator {
        friend class btree<T>;

        typedef typename btree<T>::TNode           _tnode;
        typedef btree_iterator<T>                  _self;

        typedef ptrdiff_t                          difference_type;
        typedef std::bidirectional_iterator_tag    iterator_category;
        typedef T                                  value_type;
        typedef T*                                 pointer;
        typedef T&                                 reference;

        //constructor
//        btree_iterator() : node_()  {}
        btree_iterator(_tnode *node = nullptr) : node_(node) {}

        _self& operator++();
        _self operator++(int);
        _self& operator--();
        _self operator--(int);

        reference operator*() const;
        pointer operator->() const;

        bool operator==(const _self& rhs) const;
        bool operator!=(const _self& rhs) const;// {return !operator==(rhs); }

        _tnode *node_; //*pointee_
       _tnode *parent_;
       _tnode *sibling_;

        btree<T> *tree_;

};

template <typename T>
struct const_btree_iterator {
        friend class btree_iterator<T>;
        friend class btree<T>;

        typedef const typename btree<T>::TNode       _tnode;
        typedef const_btree_iterator<T>              _self;
        typedef  btree_iterator<T>              iterator;

        typedef ptrdiff_t                            difference_type;
        typedef std::bidirectional_iterator_tag      iterator_category;
        typedef T                                    value_type;
        typedef const T*                             pointer;
        typedef const T&                             reference;

        //const_btree_iterator() : node_() {}

        const_btree_iterator(const _tnode *node = nullptr) : node_(node) {}
        const_btree_iterator(const iterator& x) : node_(x.node_) {}

        _self& operator++();
        _self operator++(int);
        _self& operator--();
        _self operator--(int);

        reference operator*() const;
        pointer operator->() const;

        bool operator==(const _self& rhs)const;
        bool operator!=(const _self& rhs) const;// {return !operator==(rhs); }
        bool operator==(const iterator&lhs) { return lhs.node_ == node_; }
        bool operator!=(const iterator&lhs) { return lhs.node_ != node_; }

        _tnode *node_; //*pointee_
        _tnode *parent_;
        _tnode *sibling_;

        btree<T> *tree_;

};

//template<typename T>
//inline bool operator==(const btree_iterator<T>& lhs, const const_btree_iterator<T>& rhs)
//  { return lhs.node_ == rhs.node_; }

//template<typename T>
//inline bool operator!=(const btree_iterator<T>& lhs, const const_btree_iterator<T>& rhs)
//  { return lhs.node_ != rhs.node_; }

#include "btree_iterator.tem"

#endif
