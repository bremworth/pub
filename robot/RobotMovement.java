

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by alex witting on 23/01/17.
 * alexwitting@gmail.com
 *
 * Description:

 The application is a simulation of a toy robot moving on a square
 tabletop, of dimensions 5 units x 5 units.

 There are no other obstructions on the table surface.

 The robot is free to roam around the surface of the table, but must be
 prevented from falling to destruction.  Any movement that would result
 in the robot falling from the table must be prevented, however further
 valid movement commands must still be allowed.

 Create an application that can read in commands of the following form -

 PLACE X,Y,F
 MOVE
 LEFT
 RIGHT
 REPORT

 Where PLACE will put the toy robot on the table in position X,Y and
 facing NORTH, SOUTH, EAST or WEST.  The origin (0,0) can be considered to
 be the SOUTH WEST most corner.

 It is required that the first command to the robot is a PLACE command,
 after that, any sequence of commands may be issued, in any order, including
 another PLACE command.  The application should discard all commands in
 the sequence until a valid PLACE command has been executed.

 Where MOVE will move the toy robot one unit forward in the direction
 it is currently facing.

 Where LEFT and RIGHT will rotate the robot 90 degrees in the specified
 direction without changing the position of the robot.

 Where REPORT will announce the X,Y and F of the robot.  This can be
 in any form, but standard output is sufficient.

 A robot that is not on the table can choose the ignore the MOVE, LEFT,
 RIGHT and REPORT commands.

 Input can be from a file, or from standard input, as the developer chooses.

 Provide test data to exercise the application.

 Constraints:

 The toy robot must not fall off the table during movement.  This also
 includes the initial placement of the toy robot.  Any move that would cause
 the robot to fall must be ignored.

 Example Input and Output:

 a)----------------
 PLACE 0,0,NORTH
 MOVE
 REPORT

 Output: 0,1,NORTH

 b)----------------
 PLACE 0,0,NORTH
 LEFT
 REPORT

 Output: 0,0,WEST
 c)----------------
 PLACE 1,2,EAST
 MOVE
 MOVE
 LEFT
 MOVE
 REPORT

 Output: 3,3,NORTH

 */
public class RobotMovement {

    String[] directions = new String[]{"NORTH","WEST","SOUTH","EAST"};
    private boolean initialised;
    private Robot robot;
    private int bounds;


    public RobotMovement() {
        bounds = 5;
        initialised = false;
    }

    public static void main(String args[]) {
        RobotMovement robot = new RobotMovement();

        //Can receive commands either from a file from the command line or via console input
        if (args.length == 1)
            robot.readCommandsFromFile(args);
        else
            robot.readCommandsFromInput();
    }

    /**
     * Receives commands from command line std input
     */
    private void readCommandsFromInput() {
        Scanner input = new Scanner(System.in);
        String command;
        while (true) { //Program is exited by command line quit
            command = input.nextLine();
            String[] commands = command.split(" ");
            run(commands);
        }
    }

    /**
     * Can read commands from a file as per the specified format
     * @param args - the command line arguments for parsing
     */
    public void readCommandsFromFile(String args[]) {
        try {
            //Get the filepath for the commands
            Path commandfile = Paths.get(args[0]);
            List<String> lines = Files.readAllLines(commandfile, Charset.defaultCharset());
            //Read all commands from the file where each command is on a line line separated by a space
            for (String line : lines) {
                String[] commands = line.split(" ");
                run(commands);
            }
        } catch (Exception e) {
            //If the commands are bad we are simply ignoring them
        }
    }

    private void run(String[] commands) {
        if (commands.length > 1) {
            initialiseRobot(commands);
        } else if  (initialised) {
            robot.command(commands[0]);
        }
    }


    /**
     * @param command - the command line arguments initialising the Robot starting position; "PLACE 0,0,NORTH"
     */
    public void initialiseRobot(String[] command) {
        String[] commands;

        //If the first command is place, initialise the robot other wise ignore the command input
        if (command[0].contains("PLACE")) {
            try {
                //PLACE 0,0,EAST
                commands = command[1].split(",");
                //Robot Location
                int x = Integer.parseInt(commands[0]);
                int y = Integer.parseInt(commands[1]);
                //Starting direction
                String startDirection = commands[2];
                //Check x,y within bounds
                if (x <= bounds && x >= 0 && y <= bounds && y >= 0
                        && Arrays.asList(directions).contains(startDirection)) {
                    //Create and initialise our new robot
                    this.robot = new Robot(x,y, startDirection, bounds);
                    //Robot was initialise correctly and can now start accepting commands
                    initialised = true;
                    return;
                }
            } catch (Exception e) {
                //If the commands are bad we are simply ignoring them
            }
        }
        //The command was bad and was not initialise; so no subsequent commands can be ordered
        initialised = false;
    }

    public Robot getRobot() {
        return robot;
    }

    public boolean getInitialised() {
        return initialised;
    }
}
