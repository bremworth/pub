

import java.util.Arrays;

/**
 * Created by alex on 23/01/17.
 */
public class Robot {
    public String direction;
    public int x, y;
    private int bounds;
    String[] directions = new String[]{"NORTH","EAST","SOUTH","WEST"};

    public Robot(int a, int b, String dir, int bounds) {
        this.x = a;
        this.y = b;
        this.direction = dir;
        this.bounds = bounds;
    }

    //Input commands
    public void command(String action) {
        switch (action) {
            case "MOVE":
                movement();
                break;
            case "LEFT":
                rotate("LEFT");
                break;
            case "RIGHT":
                rotate("RIGHT");
                break;
            case "REPORT":
                System.out.println("Output: " + x + "," + y + "," + direction);
                //"Output: 0,0,WEST"
                break;
        }
    }

    //Robot movement, which is restricted to a fixed size board
    private void movement() {
        switch (direction) {
            case "NORTH":
                if (y < bounds) y++;
                break;
            case "SOUTH":
                if (y > 0) y--;
                break;
            case "EAST":
                if (x < bounds) x++;
                break;
            case "WEST":
                if (x > 0) x--;
                break;
        }
    }

    private void rotate(String rotate) {
        int dir = Arrays.asList(directions).indexOf(direction);
        switch (rotate) {
            case "LEFT":
                dir--;
                //Compass setting so if at the end of the array we are returning to East
                if(dir < 0) dir = directions.length-1;
                break;
            case "RIGHT":
                dir++;
                //Compass setting so if at the end of the array we are returning to North
                if(dir == directions.length) dir = 0;
                break;
        }
        direction = directions[dir];
    }
}
