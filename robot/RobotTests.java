

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


/**
 * Created by alex on 23/01/17.
 */
public class RobotTests {

    public static void main(String args[]) {


        String[] testArgs1 = {"PLACE", "0,0,NORTH"};
        String[] testArgs2 = {"PLACE", "2,3,WEST"};
        String[] testArgs3 = {"PLACE", "asdf,asdf,asdf"};
        String[] testArgs4 = {"PLACE", "7,5,WEST"};
        String test_movement = "MOVE";
        String test_left = "LEFT";
        String test_right = "RIGHT";


        RobotMovement robot = new RobotMovement();
        System.out.println("Running Tests");

        System.out.println("Initialise Tests");

        System.out.println("Test 1");
        robot.initialiseRobot(testArgs1);
        assert robot.getInitialised();
        System.out.println("Test 2");
        robot.initialiseRobot(testArgs2);
        assert robot.getInitialised();
        System.out.println("Test 3");
        robot.initialiseRobot(testArgs3);
        assert !robot.getInitialised();
        System.out.println("Test 4");
        robot.initialiseRobot(testArgs4);
        assert !robot.getInitialised();

        System.out.println("Command Tests");

        System.out.println("Test 1");
        robot.initialiseRobot(testArgs1); //PLACE, 0,0,NORTH
        assert robot.getInitialised();

        System.out.println("Test 2");
        robot.getRobot().command(test_movement); //0,1,NORTH
        assert robot.getRobot().x == 0;
        assert robot.getRobot().y == 1;
        System.out.println("Test 3");
        robot.getRobot().command(test_left);
        assert robot.getRobot().direction.contains("WEST");
        System.out.println("Test 4");
        robot.getRobot().command(test_right);
        assert robot.getRobot().direction.contains("NORTH");
        System.out.println("Test 5");
        robot.getRobot().command("BAD_COMMAND"); //No action
        robot.getRobot().command("REPORT"); //1,0,NORTH

        System.out.println("Movement Tests");

        System.out.println("Test 1");

        robot.initialiseRobot(testArgs1); // 0,0,NORTH
        robot.getRobot().command(test_movement); //MOVE 0,1,NORTH
        robot.getRobot().command(test_movement); //MOVE 0,2,NORTH
        robot.getRobot().command(test_right); //RIGHT 0,2,EAST
        robot.getRobot().command(test_movement); //MOVE 1,2,EAST
        robot.getRobot().command(test_left); //LEFT 1,2,NORTH
        robot.getRobot().command(test_movement); //MOVE 1,3,NORTH
        robot.getRobot().command(test_movement); //MOVE 1,4,NORTH
        robot.getRobot().command("REPORT");

        assert robot.getRobot().x == 1;
        assert robot.getRobot().y == 4;
        assert robot.getRobot().direction.contains("NORTH");

        System.out.println("Test 2");

        robot.initialiseRobot(testArgs2); // 2,3,WEST
        robot.getRobot().command(test_movement); //MOVE 1,3,WEST
        robot.getRobot().command(test_movement); //MOVE 0,3,WEST
        robot.getRobot().command(test_right); //RIGHT 0,3,NORTH
        robot.getRobot().command(test_movement); //MOVE 0,4,NORTH
        robot.getRobot().command(test_left); //LEFT 0,4,WEST
        robot.getRobot().command(test_left); //LEFT 0,4,SOUTH
        robot.getRobot().command(test_movement); //MOVE 0,3,SOUTH
        robot.getRobot().command(test_movement); //MOVE 0,2,SOUTH
        robot.getRobot().command("REPORT");

        assert robot.getRobot().x == 0;
        assert robot.getRobot().y == 2;
        assert robot.getRobot().direction.contains("SOUTH");

        System.out.println("File Input Tests");

        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));

        robot = new RobotMovement();
        robot.readCommandsFromFile(new String[]{"testRobot"});
        assert outContent.toString().contains("Output: 1,4,NORTH\n");

        robot = new RobotMovement();
        robot.readCommandsFromFile(new String[]{"testRobot1"});
        assert outContent.toString().contains("Output: 0,2,SOUTH\n");

        robot = new RobotMovement();
        robot.readCommandsFromFile(new String[]{"testRobot2"});
        assert outContent.toString().contains("Output: 1,4,NORTH\nOutput: 0,2,SOUTH\n");

        robot = new RobotMovement();
        robot.readCommandsFromFile(new String[]{"testRobot3"});
        assert outContent.toString().contains("Output: 0,0,NORTH\n");

        robot = new RobotMovement();
        robot.readCommandsFromFile(new String[]{"testRobot4"});
        assert outContent.toString().contains("Output: 0,0,NORTH\n");


        System.out.println("All Tests Completed");
    }
}
