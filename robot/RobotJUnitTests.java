

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;


/**
 * Created by alex on 23/01/17.
 */
public class RobotJUnitTests {

    RobotMovement robot;
    String test_movement = "MOVE";
    String test_left = "LEFT";
    String test_right = "RIGHT";
    String[] testArgs1 = {"PLACE", "0,0,NORTH"};
    String[] testArgs2 = {"PLACE", "2,3,WEST"};
    String[] testArgs3 = {"PLACE", "asdf,asdf,asdf"};
    String[] testArgs4 = {"PLACE", "7,5,WEST"};

    @Before
    public void setup() {
        robot = new RobotMovement();
    }

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }


    @Test
    public void initialiseRobotTest() {

        robot.initialiseRobot(testArgs1);
        assertTrue(robot.getInitialised());
        robot.initialiseRobot(testArgs2);
        assertTrue(robot.getInitialised());
        robot.initialiseRobot(testArgs3);
        assertFalse(robot.getInitialised());
        robot.initialiseRobot(testArgs4);
        assertFalse(robot.getInitialised());

    }

    @Test
    public void commandTests() {

        robot.initialiseRobot(testArgs1); //PLACE, 0,0,NORTH
        assertTrue(robot.getInitialised());

        robot.getRobot().command(test_movement); //0,1,NORTH
        assertEquals(robot.getRobot().x, 0);
        assertEquals(robot.getRobot().y, 1);
        robot.getRobot().command(test_left);
        assertTrue(robot.getRobot().direction.contains("WEST"));
        robot.getRobot().command(test_right);
        assertTrue(robot.getRobot().direction.contains("NORTH"));
        robot.getRobot().command("BAD_COMMAND"); //No action
        robot.getRobot().command("REPORT"); //1,0,NORTH

    }

    @Test
    public void movementTests() {

        robot.initialiseRobot(testArgs1); // 0,0,NORTH
        robot.getRobot().command(test_movement); //MOVE 0,1,NORTH
        robot.getRobot().command(test_movement); //MOVE 0,2,NORTH
        robot.getRobot().command(test_right); //RIGHT 0,2,EAST
        robot.getRobot().command(test_movement); //MOVE 1,2,EAST
        robot.getRobot().command(test_left); //LEFT 1,2,NORTH
        robot.getRobot().command(test_movement); //MOVE 1,3,NORTH
        robot.getRobot(). command(test_movement); //MOVE 1,4,NORTH
        robot.getRobot(). command("REPORT");

        assertEquals(robot.getRobot().x, 1);
        assertEquals(robot.getRobot().y, 4);
        assertTrue(robot.getRobot().direction.contains("NORTH"));

        robot.initialiseRobot(testArgs2); // 2,3,WEST
        robot.getRobot().command(test_movement); //MOVE 1,3,WEST
        robot.getRobot().command(test_movement); //MOVE 0,3,WEST
        robot.getRobot().command(test_right); //RIGHT 0,3,NORTH
        robot.getRobot().command(test_movement); //MOVE 0,4,NORTH
        robot.getRobot().command(test_left); //LEFT 0,4,WEST
        robot.getRobot().command(test_left); //LEFT 0,4,SOUTH
        robot.getRobot().command(test_movement); //MOVE 0,3,SOUTH
        robot.getRobot(). command(test_movement); //MOVE 0,2,SOUTH
        robot.getRobot(). command("REPORT");

        assertEquals(robot.getRobot().x, 0);
        assertEquals(robot.getRobot().y, 2);
        assertTrue( robot.getRobot().direction.contains("SOUTH"));

    }

    @Test
    public void fileInputTests() {
        robot.readCommandsFromFile(new String[]{"testRobot"});
        assertEquals("Output: 1,4,NORTH\n", outContent.toString());
    }

    @Test
    public void fileInputTests1() {
        robot.readCommandsFromFile(new String[]{"testRobot1"});
        assertEquals("Output: 0,2,SOUTH\n", outContent.toString());
    }

    @Test
    public void fileInputTests2() {
        robot.readCommandsFromFile(new String[]{"testRobot2"});
        assertEquals("Output: 1,4,NORTH\nOutput: 0,2,SOUTH\n", outContent.toString());
    }


    @Test
    public void fileInputTests3() {
        robot.readCommandsFromFile(new String[]{"testRobot3"});
        assertTrue(robot.getInitialised());
        assertEquals("Output: 0,0,NORTH\n", outContent.toString());
    }


    @Test
    public void fileInputTests4() {
        robot.readCommandsFromFile(new String[]{"testRobot4"});
        assertTrue(robot.getInitialised());
        assertEquals("Output: 0,0,NORTH\n", outContent.toString());
    }
}
